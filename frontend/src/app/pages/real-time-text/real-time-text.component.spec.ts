import { ComponentFixture, TestBed } from '@angular/core/testing'

import { RealTimeTextViewComponent } from './real-time-text.component'

describe('RealTimeTextViewComponent', () => {
  let component: RealTimeTextViewComponent
  let fixture: ComponentFixture<RealTimeTextViewComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RealTimeTextViewComponent],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(RealTimeTextViewComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
