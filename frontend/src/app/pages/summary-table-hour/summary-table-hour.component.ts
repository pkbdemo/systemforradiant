import { Component, OnInit } from '@angular/core'
import { LayoutService } from '../layout/services/layout.service'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'
import { environment } from '../../../environments/environment'
import {
  dropdownItem,
  Person,
} from '../summary-table-hour/parameterSettingModel'
import { NzButtonSize } from 'ng-zorro-antd/button'
import Keyboard from 'simple-keyboard'
import { NzMessageService } from 'ng-zorro-antd/message'
import { Router } from '@angular/router'
import moment from 'moment'
interface navBtnItem {
  Code_id: string
  Code_Name: string
}
@Component({
  selector: 'app-summary-table-hour',
  templateUrl: './summary-table-hour.component.html',
  styleUrls: ['./summary-table-hour.component.css'],
})
export class SummaryTableHourViewComponent implements OnInit {
  interval: any
  machineNum: string = '1'
  cold_input: number = 0
  cold_output: number = 0
  cool_down_input: number = 0
  cool_down_output: number = 0
  latestDataTime: string = ''
  placeHolder: string[] = ['開始時間', '結束時間']
  listOfData: any
  dateRange: Date[] = []
  error_temp1 = ''
  error_temp2 = ''
  error_temp3 = ''

  date: Date[] = []
  constructor(
    private layoutService: LayoutService,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private message: NzMessageService,
    private router: Router,
  ) {}
  async ngOnInit() {
    let formData = new FormData()
    formData.set('param1', this.machineNum)
    this.httpClient
      .post<any>(
        environment.apiServerURL + 'Content/QueryHourlyReport',
        formData,
      )
      .subscribe({
        next: (res) => {
          this.listOfData = res
        },
        error: (err) => {
          console.error(err)
          this.toastr.error('request failed')
        },
      })

    let formData2 = new FormData()
    formData2.set('param1', '')
    this.httpClient
      .post<any>(environment.apiServerURL + 'Setting/Query', formData2)
      .subscribe({
        next: (res) => {
          this.error_temp1 = res.error_Temp1
          this.error_temp2 = res.error_Temp2
          this.error_temp3 = res.error_Temp3
        },
        error: (err) => {
          console.error(err)
          this.toastr.error('request failed')
        },
      })
  }
  search() {
    let formData = new FormData()
    formData.set('param1', this.machineNum)
    formData.set(
      'startDate',
      !!this.date[0] ? moment(this.date[0]).format('YYYY-MM-DD HH') : '',
    )
    formData.set(
      'endDate',
      !!this.date[1] ? moment(this.date[1]).format('YYYY-MM-DD HH') : '',
    )
    this.httpClient
      .post<any>(
        environment.apiServerURL + 'Content/QueryHourlyReport',
        formData,
      )
      .subscribe({
        next: (res) => {
          this.listOfData = res
        },
        error: (err) => {
          console.error(err)
          this.toastr.error('request failed')
        },
      })
  }
  onChange(result: Date[]): void {
    console.log('onChange: ', result)
    this.date = result
  }

  download() {
    let formData = new FormData()
    formData.set('param1', this.machineNum)
    formData.set(
      'startDate',
      !!this.date[0] ? moment(this.date[0]).format('YYYY-MM-DD HH') : '',
    )
    formData.set(
      'endDate',
      !!this.date[1] ? moment(this.date[1]).format('YYYY-MM-DD HH') : '',
    )
    this.httpClient
      .post<any>(
        environment.apiServerURL + 'Content/DownloadHourlyExcel',
        formData,
      )
      .subscribe({
        next: (res) => {},
        error: (err) => {
          console.error(err)
          this.toastr.error('request failed')
        },
      })
    this.createMessage('success')
  }

  changeMachineNum() {
    let formData = new FormData()
    formData.set('param1', this.machineNum)
    formData.set(
      'startDate',
      !!this.date[0] ? moment(this.date[0]).format('YYYY-MM-DD HH') : '',
    )
    formData.set(
      'endDate',
      !!this.date[1] ? moment(this.date[1]).format('YYYY-MM-DD HH') : '',
    )
    this.httpClient
      .post<any>(
        environment.apiServerURL + 'Content/QueryHourlyReport',
        formData,
      )
      .subscribe({
        next: (res) => {
          this.listOfData = res
        },
        error: (err) => {
          console.error(err)
          this.toastr.error('request failed')
        },
      })
  }

  createMessage(type: string): void {
    if (type == 'success') this.message.create(type, '下載成功')
    if (type == 'error') this.message.create(type, '下載失敗，格式不符')
  }
}
