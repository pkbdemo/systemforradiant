import { ComponentFixture, TestBed } from '@angular/core/testing'

import { RealTimeTextWeekViewComponent } from './real-time-text-week.component'

describe('RealTimeTextWeekViewComponent', () => {
  let component: RealTimeTextWeekViewComponent
  let fixture: ComponentFixture<RealTimeTextWeekViewComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RealTimeTextWeekViewComponent],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(RealTimeTextWeekViewComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
