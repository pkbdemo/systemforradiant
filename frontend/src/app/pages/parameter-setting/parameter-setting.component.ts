import { Component, OnInit } from '@angular/core'
import { LayoutService } from '../layout/services/layout.service'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'
import { environment } from '../../../environments/environment'
import { dropdownItem } from '../parameter-setting/parameterSettingModel'
import { NzButtonSize } from 'ng-zorro-antd/button'
import Keyboard from 'simple-keyboard'
import { NzMessageService } from 'ng-zorro-antd/message'
import { Router } from '@angular/router'
import moment from 'moment'
interface navBtnItem {
  Code_id: string
  Code_Name: string
}
@Component({
  selector: 'app-parameter-setting',
  templateUrl: './parameter-setting.component.html',
  styleUrls: ['./parameter-setting.component.css'],
})
export class ParameterSettingViewComponent implements OnInit {
  startValue: Date | null = null
  endValue: Date | null = null

  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.endValue) {
      return false
    }
    return startValue.getTime() > this.endValue.getTime()
  }

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.startValue) {
      return false
    }
    return endValue.getTime() <= this.startValue.getTime()
  }

  handleStartOpenChange(open: boolean): void {
    if (!open) {
      // debugger
      this.endValue = moment(this.startValue).add(30, 'days').toDate()
    }
    console.log('handleStartOpenChange', open)
  }

  // handleEndOpenChange(open: boolean): void {
  //   console.log('handleEndOpenChange', open);
  // }

  userName: string
  userId: string
  size: NzButtonSize = 'large'
  isModalVisible: boolean = false
  queryValueList: any
  portNameValue: string
  baudRateValue: string
  dataBitsValue: string
  stopBitsValue: string
  timerValue: string
  parityValue: string
  usb_dir_location: string
  cold_input1: string
  cold_output1: string
  cool_down_input1: string
  cool_down_output1: string
  cold_input2: string
  cold_output2: string
  cool_down_input2: string
  cool_down_output2: string
  cold_input3: string
  cold_output3: string
  cool_down_input3: string
  cool_down_output3: string
  gate1_open: string
  gate1_close: string
  gate2_open: string
  gate2_close: string
  gate3_open: string
  gate3_close: string
  ThirdSensor: string

  timer: dropdownItem[] = [
    {
      name: '1',
      code: '1',
    },
    {
      name: '3',
      code: '3',
    },
    {
      name: '5',
      code: '5',
    },
    {
      name: '10',
      code: '10',
    },
    {
      name: '30',
      code: '30',
    },
  ]
  portName: dropdownItem[] = [
    {
      name: 'COM1',
      code: 'COM1',
    },
    {
      name: 'COM2',
      code: 'COM2',
    },
    {
      name: 'COM3',
      code: 'COM3',
    },
    {
      name: 'COM4',
      code: 'COM4',
    },
    {
      name: 'COM5',
      code: 'COM5',
    },
  ]
  baudRate: dropdownItem[] = [
    {
      name: '2400',
      code: '2400',
    },
    {
      name: '4800',
      code: '4800',
    },
    {
      name: '9600',
      code: '9600',
    },
    {
      name: '19200',
      code: '19200',
    },
    {
      name: '38400',
      code: '38400',
    },
    {
      name: '76800',
      code: '76800',
    },
  ]
  dataBits: dropdownItem[] = [
    {
      name: '7',
      code: '7',
    },
    {
      name: '8',
      code: '8',
    },
  ]
  stopBits: dropdownItem[] = [
    {
      name: 'None',
      code: 'None',
    },
    {
      name: 'One',
      code: 'One',
    },
    {
      name: 'Two',
      code: 'Two',
    },
    {
      name: 'OnePointFive',
      code: 'OnePointFive',
    },
  ]
  parity: dropdownItem[] = [
    {
      name: 'None',
      code: 'None',
    },
    {
      name: 'Odd',
      code: 'Odd',
    },
    {
      name: 'Even',
      code: 'Even',
    },
    {
      name: 'Mark',
      code: 'Mark',
    },
    {
      name: 'Space',
      code: 'Space',
    },
  ]
  enableThirdSensor: dropdownItem[] = [
    {
      name: 'True',
      code: 'true',
    },
    {
      name: 'False',
      code: 'false',
    },
  ]
  navBtn: navBtnItem[] = [
    {
      Code_id: '',
      Code_Name: 'All',
    },
  ]
  //鍵盤相關
  threshhold_temp_setting1 = ''
  threshhold_temp_setting2 = ''
  threshhold_temp_setting3 = ''
  error_temp1 = ''
  error_temp2 = ''
  error_temp3 = ''
  error_down1 = ''
  error_down2 = ''
  error_down3 = ''
  keyboard: Keyboard
  keyboard_hidden: boolean = true

  asdf() {
    // this.keyboard.clearInput()
    var tt = this.keyboard.getInput('deult')
    var tt = this.keyboard.getInput('dlt')
  }

  timerChange(event: string) {
    this.timerValue = event
  }

  dataBitsChange(event: string) {
    this.dataBitsValue = event
  }

  baudRateChange(event: string) {
    this.baudRateValue = event
  }

  ngAfterViewInit() {
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 1),
      onKeyPress: (button) => this.onKeyPress(button),
    })
  }

  clickCold_input1() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 1),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'deult',
    })
    this.keyboard.setInput(this.cold_input1)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
    this.isModalVisible = true
  }
  clickCold_output1() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 2),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.cold_output1)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }
  clickCool_down_input1() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 4),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.cool_down_input1)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }
  clickCool_down_output1() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 5),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.cool_down_output1)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }
  clickCold_input2() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 6),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'deult',
    })
    this.keyboard.setInput(this.cold_input2)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
    this.isModalVisible = true
  }
  clickCold_output2() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 7),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.cold_output2)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }
  clickCool_down_input2() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 8),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.cool_down_input2)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }
  clickCool_down_output2() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 9),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.cool_down_output2)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }
  clickCold_input3() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 10),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'deult',
    })
    this.keyboard.setInput(this.cold_input3)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
    this.isModalVisible = true
  }
  clickCold_output3() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 11),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.cold_output3)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }
  clickCool_down_input3() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 12),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.cool_down_input3)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }
  clickCool_down_output3() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 13),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.cool_down_output3)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }
  clickGate1_open() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 14),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.gate1_open)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }
  clickGate1_close() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 15),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.gate1_close)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }
  clickGate2_open() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 16),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.gate2_open)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }
  clickGate2_close() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 17),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.gate2_close)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }
  clickGate3_open() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 18),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.gate3_open)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }
  clickGate3_close() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 19),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.gate3_close)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }
  clickUsb_dir_location() {
    this.keyboard.destroy()
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input, 3),
      onKeyPress: (button) => this.onKeyPress(button),
      inputName: 'dlt',
    })
    this.keyboard.setInput(this.usb_dir_location)
    // this.keyboard.destroy()
    this.keyboard_hidden = false
  }

  onChange = (input: string, num: number) => {
    if (input.length <= 16) {
      if (num == 1) this.cold_input1 = input
      if (num == 2) this.cold_output1 = input
      if (num == 3) this.usb_dir_location = input
      if (num == 4) this.cool_down_input1 = input
      if (num == 5) this.cool_down_output1 = input
      if (num == 6) this.cold_input2 = input
      if (num == 7) this.cold_output2 = input
      if (num == 8) this.cool_down_input2 = input
      if (num == 9) this.cool_down_output2 = input
      if (num == 10) this.cold_input3 = input
      if (num == 11) this.cold_output3 = input
      if (num == 12) this.cool_down_input3 = input
      if (num == 13) this.cool_down_output3 = input
      if (num == 14) this.gate1_open = input
      if (num == 15) this.gate1_close = input
      if (num == 16) this.gate2_open = input
      if (num == 17) this.gate2_close = input
      if (num == 18) this.gate3_open = input
      if (num == 19) this.gate3_close = input
      console.log('Input changed', input)
    } else if (input.length > 16) {
      if (num == 3) this.usb_dir_location = input
    }
  }

  onKeyPress = (button: string) => {
    console.log('Button pressed', button)

    /**
     * If you want to handle the shift and caps lock buttons
     */
    if (button === '{shift}' || button === '{lock}') this.handleShift()
  }

  onInputChange = (event: any) => {
    this.keyboard.setInput(event.target.value.length)
  }

  handleShift = () => {
    let currentLayout = this.keyboard.options.layoutName
    let shiftToggle = currentLayout === 'default' ? 'shift' : 'default'

    this.keyboard.setOptions({
      layoutName: shiftToggle,
    })
  }

  constructor(
    private layoutService: LayoutService,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private message: NzMessageService,
    private router: Router,
  ) {}

  async ngOnInit() {
    let formData = new FormData()
    formData.set('param1', '')
    this.httpClient
      .post<any>(environment.apiServerURL + 'Setting/Query', formData)
      .subscribe({
        next: (res) => {
          this.queryValueList = res
          this.portNameValue = res.port_Name
          this.baudRateValue = res.baud_Rate
          this.dataBitsValue = res.data_Bits
          this.stopBitsValue = res.stop_Bits
          this.parityValue = res.parity
          this.threshhold_temp_setting1 = res.threshhold_Temp_Setting1
          this.threshhold_temp_setting2 = res.threshhold_Temp_Setting2
          this.threshhold_temp_setting3 = res.threshhold_Temp_Setting3
          this.ThirdSensor = res.enable_Third_Sensor
          this.usb_dir_location = res.usb_Dir_Location
          this.cold_input1 = res.cold_Input1.replaceAll('-', '')
          this.cold_output1 = res.cold_Output1.replaceAll('-', '')
          this.cool_down_input1 = res.cool_Down_Input1.replaceAll('-', '')
          this.cool_down_output1 = res.cool_Down_Output1.replaceAll('-', '')
          this.cold_input2 = res.cold_Input2.replaceAll('-', '')
          this.cold_output2 = res.cold_Output2.replaceAll('-', '')
          this.cool_down_input2 = res.cool_Down_Input2.replaceAll('-', '')
          this.cool_down_output2 = res.cool_Down_Output2.replaceAll('-', '')
          this.cold_input3 = res.cold_Input3.replaceAll('-', '')
          this.cold_output3 = res.cold_Output3.replaceAll('-', '')
          this.cool_down_input3 = res.cool_Down_Input3.replaceAll('-', '')
          this.cool_down_output3 = res.cool_Down_Output3.replaceAll('-', '')
          this.gate1_open = res.gate1_Open.replaceAll('-', '')
          this.gate1_close = res.gate1_Close.replaceAll('-', '')
          this.gate2_open = res.gate2_Open.replaceAll('-', '')
          this.gate2_close = res.gate2_Close.replaceAll('-', '')
          this.gate3_open = res.gate3_Open.replaceAll('-', '')
          this.gate3_close = res.gate3_Close.replaceAll('-', '')
          this.error_temp1 = res.error_Temp1
          this.error_temp2 = res.error_Temp2
          this.error_temp3 = res.error_Temp3
          this.error_down1 = res.error_Down1
          this.error_down2 = res.error_Down2
          this.error_down3 = res.error_Down3
          this.timerValue = res.timer
        },
        error: (err) => {
          console.error(err)
          this.toastr.error('request failed')
        },
      })
  }
  splitStr(param: string): string {
    var temp = param.split('')
    var result = ''
    for (let n = 0; n < 16; n += 2) {
      result += temp[n] + temp[n + 1] + '-'
    }
    result = result.substring(0, result.length - 1)
    return result
  }
  update() {
    if (!this.checkFormate()) {
      this.createMessage('error')
      return
    }

    var param = JSON.parse(sessionStorage.getItem('intervalItems'))['item']
    clearInterval(param)
    sessionStorage.clear()

    var interval = setInterval(() => {
      this.httpClient
        .get<any>(environment.apiServerURL + 'Setting/TriggerJob')
        .subscribe({
          next: (res) => {},
          error: (err) => {
            console.error(err)
            this.toastr.error('request failed')
          },
        })
    }, Number(this.timerValue) * 1000 * 60)
    sessionStorage.setItem(
      'intervalItems',
      JSON.stringify({
        item: interval,
      }),
    )

    let formData = new FormData()
    formData.set('Port_Name', this.portNameValue)
    formData.set('Baud_Rate', this.baudRateValue.toString())
    formData.set('Data_Bits', this.dataBitsValue.toString())
    formData.set('Stop_Bits', this.stopBitsValue)
    formData.set('Parity', this.parityValue)
    formData.set('Threshhold_Temp_Setting1', this.threshhold_temp_setting1)
    formData.set('Threshhold_Temp_Setting2', this.threshhold_temp_setting2)
    formData.set('Threshhold_Temp_Setting3', this.threshhold_temp_setting3)
    formData.set('Enable_Third_Sensor', this.ThirdSensor)
    formData.set('Usb_Dir_Location', this.usb_dir_location)
    formData.set('Cold_Input1', this.splitStr(this.cold_input1).toUpperCase())
    formData.set('Cold_Output1', this.splitStr(this.cold_output1).toUpperCase())
    formData.set(
      'Cool_Down_Input1',
      this.splitStr(this.cool_down_input1).toUpperCase(),
    )
    formData.set(
      'Cool_Down_Output1',
      this.splitStr(this.cool_down_output1).toUpperCase(),
    )
    formData.set('Cold_Input2', this.splitStr(this.cold_input2).toUpperCase())
    formData.set('Cold_Output2', this.splitStr(this.cold_output2).toUpperCase())
    formData.set(
      'Cool_Down_Input2',
      this.splitStr(this.cool_down_input2).toUpperCase(),
    )
    formData.set(
      'Cool_Down_Output2',
      this.splitStr(this.cool_down_output2).toUpperCase(),
    )
    formData.set('Cold_Input3', this.splitStr(this.cold_input3).toUpperCase())
    formData.set('Cold_Output3', this.splitStr(this.cold_output3).toUpperCase())
    formData.set(
      'Cool_Down_Input3',
      this.splitStr(this.cool_down_input3).toUpperCase(),
    )
    formData.set(
      'Cool_Down_Output3',
      this.splitStr(this.cool_down_output3).toUpperCase(),
    )
    debugger
    formData.set('Gate1_Open', this.splitStr(this.gate1_open).toUpperCase())
    formData.set('Gate1_Close', this.splitStr(this.gate1_close).toUpperCase())
    formData.set('Gate2_Open', this.splitStr(this.gate2_open).toUpperCase())
    formData.set('Gate2_Close', this.splitStr(this.gate2_close).toUpperCase())
    formData.set('Gate3_Open', this.splitStr(this.gate3_open).toUpperCase())
    formData.set('Gate3_Close', this.splitStr(this.gate3_close).toUpperCase())
    formData.set('Error_Temp1', this.error_temp1)
    formData.set('Error_Temp2', this.error_temp2)
    formData.set('Error_Temp3', this.error_temp3)
    formData.set('Error_Down1', this.error_down1)
    formData.set('Error_Down2', this.error_down2)
    formData.set('Error_Down3', this.error_down3)
    formData.set('Timer', this.timerValue.toString())
    this.httpClient
      .post<any>(environment.apiServerURL + 'Setting/Update', formData)
      .subscribe({
        next: (res) => {
          if (res) {
            this.router.navigate(['parametersetting'])
          }
        },
        error: (err) => {
          console.error(err)
          this.toastr.error('request failed')
        },
      })
    this.createMessage('success')
  }
  checkFormate(): boolean {
    if (
      this.cold_input1.length != 16 ||
      this.cold_output1.length != 16 ||
      this.cool_down_input1.length != 16 ||
      this.cool_down_output1.length != 16 ||
      this.cold_input2.length != 16 ||
      this.cold_output2.length != 16 ||
      this.cool_down_input2.length != 16 ||
      this.cool_down_output2.length != 16 ||
      this.cold_input3.length != 16 ||
      this.cold_output3.length != 16 ||
      this.cool_down_input3.length != 16 ||
      this.cool_down_output3.length != 16 ||
      this.gate1_open.length != 16 ||
      this.gate1_close.length != 16 ||
      this.gate2_open.length != 16 ||
      this.gate2_close.length != 16 ||
      this.gate3_open.length != 16 ||
      this.gate3_close.length != 16
    )
      return false
    else return true
  }

  createMessage(type: string): void {
    if (type == 'success') this.message.create(type, '儲存成功')
    if (type == 'error') this.message.create(type, '儲存失敗，格式不符')
  }
}
