import { Component, OnInit } from '@angular/core'
import { LayoutService } from '../layout/services/layout.service'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'
import { environment } from '../../../environments/environment'
import * as echarts from 'echarts'
import { NzButtonSize } from 'ng-zorro-antd/button'
import * as moment from 'moment'

interface navBtnItem {
  Code_id: string
  Code_Name: string
}
@Component({
  selector: 'app-real-time-view',
  templateUrl: './real-time-view.component.html',
  styleUrls: ['./real-time-view.component.css'],
})
export class RealTimeViewComponent implements OnInit {
  onInitFlag: boolean = true
  machineNum: string = '1'
  type: string = ''
  userName: string
  userId: string
  navCheckBtn: number = 0
  InProfressCount: number = 0
  InProfressCountAll: number = 0
  size: NzButtonSize = 'large'
  isShowVector: boolean = true
  isShowUserVector: boolean = true
  selectedDiv: string = 'initial'
  listSelected: string = 'My List'
  chart1: any
  chart2: any
  chart3: any
  chart4: any
  myChartGlobal1: echarts.ECharts
  myChartGlobal2: echarts.ECharts
  myChartGlobal3: echarts.ECharts
  myChartGlobal4: echarts.ECharts
  optionGlobal1: any
  optionGlobal2: any
  optionGlobal3: any
  optionGlobal4: any
  categoriesGlobal1: any
  categoriesGlobal2: any
  latestDataTime: any
  dataGlobal1: any
  categories = (function () {
    let now = new Date()
    let res = []
    // let len = 30
    // while (len--) {
    //   res.unshift(now.toLocaleTimeString().replace(/^\D*/, ''))
    //   now = new Date(+now - 2000)
    // }
    return res
  })()
  categories2 = (function () {
    let res = []
    return res
  })()
  data1: number[] = (function () {
    let res = []
    // let len = 30
    // while (len--) {
    //   res.push(Math.round(Math.random() * 1000))
    // }
    return res
  })()
  data2: number[] = (function () {
    let res = []
    return res
  })()
  data3: number[] = (function () {
    let res = []
    return res
  })()
  data4: number[] = (function () {
    let res = []
    return res
  })()
  // const machineChange: boolean = false
  public machineChange: boolean = false
  navBtn: navBtnItem[] = [
    {
      Code_id: '',
      Code_Name: 'All',
    },
  ]

  changeMachineNum() {
    this.machineChange = true
  }

  constructor(
    private layoutService: LayoutService,
    private httpClient: HttpClient,
    private toastr: ToastrService,
  ) {}

  async ngOnInit() {
    let formData = new FormData()
    formData.set('param1', '1')
    this.httpClient
      .post<any>(environment.apiServerURL + 'Content/QueryAll', formData)
      .subscribe({
        next: (res) => {
          res.forEach((element) => {
            this.categories.push(
              moment(element.upd_time.substring(0, 19)).format('HH:mm'),
            )
            this.categories2.push(
              moment(element.upd_time.substring(0, 19)).format('MM-DD'),
            )
            this.data1.push(element.cool_input)
            this.data2.push(element.cool_output)
            this.data3.push(element.cool_down_input)
            this.data4.push(element.cool_down_output)
          })
          this.latestDataTime = res[res.length - 1].upd_time
        },
        error: (err) => {
          console.error(err)
          this.toastr.error('request failed')
        },
      })
    //#region
    var app: any = {}
    type EChartsOption = echarts.EChartsOption

    var chartDom = document.getElementById('main')!
    var chartDom2 = document.getElementById('main2')!
    var chartDom3 = document.getElementById('main3')!
    var chartDom4 = document.getElementById('main4')!

    var myChart = echarts.init(chartDom)
    var myChart2 = echarts.init(chartDom2)
    var myChart3 = echarts.init(chartDom3)
    var myChart4 = echarts.init(chartDom4)

    const count: number[] = (function () {
      let res = []
      return res
    })()

    var option: EChartsOption
    var option2: EChartsOption
    var option3: EChartsOption
    var option4: EChartsOption

    option = {
      title: {
        text: '冰水出水',
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          label: {
            backgroundColor: '#283b56',
          },
        },
      },
      legend: {},
      toolbox: {
        show: false,
        feature: {
          dataView: { readOnly: false },
          restore: {},
          saveAsImage: {},
        },
      },
      dataZoom: {
        show: false,
        start: 0,
        end: 100,
      },
      xAxis: [
        {
          type: 'category',
          boundaryGap: true,
          data: this.categories,
          // axisLine: {
          //   lineStyle: {
          //     color: '#0bfc03',
          //   },
          // },
        },
        {
          type: 'category',
          boundaryGap: true,
          data: this.categories2,
        },
      ],
      yAxis: [
        {
          type: 'value',
          scale: true,
          name: '',
          max: 80,
          min: 0,
          boundaryGap: [0.2, 0.2],
        },
        {
          type: 'value',
          scale: true,
          name: '°C',
          max: 80,
          min: 0,
          boundaryGap: [0.2, 0.2],
        },
      ],
      series: [
        {
          name: '溫度長條',
          type: 'bar',
          xAxisIndex: 1,
          yAxisIndex: 1,
          data: this.data1,
        },
        {
          name: '溫度折線',
          type: 'line',
          data: this.data1,
          color: '#FF8000',
        },
      ],
    }

    option2 = {
      title: {
        text: '冰水入水',
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          label: {
            backgroundColor: '#283b56',
          },
        },
      },
      legend: {},
      toolbox: {
        show: false,
        feature: {
          dataView: { readOnly: false },
          restore: {},
          saveAsImage: {},
        },
      },
      dataZoom: {
        show: false,
        start: 0,
        end: 100,
      },
      xAxis: [
        {
          type: 'category',
          boundaryGap: true,
          data: this.categories,
        },
        {
          type: 'category',
          boundaryGap: true,
          data: this.categories2,
        },
      ],
      yAxis: [
        {
          type: 'value',
          scale: true,
          name: '',
          max: 80,
          min: 0,
          boundaryGap: [0.2, 0.2],
        },
        {
          type: 'value',
          scale: true,
          name: '°C',
          max: 80,
          min: 0,
          boundaryGap: [0.2, 0.2],
        },
      ],
      series: [
        {
          name: '溫度長條',
          type: 'bar',
          xAxisIndex: 1,
          yAxisIndex: 1,
          data: this.data2,
        },
        {
          name: '溫度折線',
          type: 'line',
          data: this.data2,
          color: '#FF8000',
        },
      ],
    }

    option3 = {
      title: {
        text: '冷卻水出水',
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          label: {
            backgroundColor: '#283b56',
          },
        },
      },
      legend: {},
      toolbox: {
        show: false,
        feature: {
          dataView: { readOnly: false },
          restore: {},
          saveAsImage: {},
        },
      },
      dataZoom: {
        show: false,
        start: 0,
        end: 100,
      },
      xAxis: [
        {
          type: 'category',
          boundaryGap: true,
          data: this.categories,
        },
        {
          type: 'category',
          boundaryGap: true,
          data: this.categories2,
        },
      ],
      yAxis: [
        {
          type: 'value',
          scale: true,
          name: '',
          max: 80,
          min: 0,
          boundaryGap: [0.2, 0.2],
        },
        {
          type: 'value',
          scale: true,
          name: '°C',
          max: 80,
          min: 0,
          boundaryGap: [0.2, 0.2],
        },
      ],
      series: [
        {
          name: '溫度長條',
          type: 'bar',
          xAxisIndex: 1,
          yAxisIndex: 1,
          data: this.data3,
        },
        {
          name: '溫度折線',
          type: 'line',
          data: this.data3,
          color: '#FF8000',
        },
      ],
    }

    option4 = {
      title: {
        text: '冷卻水入水',
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          label: {
            backgroundColor: '#283b56',
          },
        },
      },
      legend: {},
      toolbox: {
        show: false,
        feature: {
          dataView: { readOnly: false },
          restore: {},
          saveAsImage: {},
        },
      },
      dataZoom: {
        show: false,
        start: 0,
        end: 100,
      },
      xAxis: [
        {
          type: 'category',
          boundaryGap: true,
          data: this.categories,
        },
        {
          type: 'category',
          boundaryGap: true,
          data: this.categories2,
        },
      ],
      yAxis: [
        {
          type: 'value',
          scale: true,
          name: '',
          max: 80,
          min: 0,
          boundaryGap: [0.2, 0.2],
        },
        {
          type: 'value',
          scale: true,
          name: '°C',
          max: 80,
          min: 0,
          boundaryGap: [0.2, 0.2],
        },
      ],
      series: [
        {
          name: '溫度長條',
          type: 'bar',
          xAxisIndex: 1,
          yAxisIndex: 1,
          data: this.data4,
        },
        {
          name: '溫度折線',
          type: 'line',
          data: this.data4,
          color: '#FF8000',
        },
      ],
    }
    app.count = 30

    var interval = setInterval(() => {
      let axisData = new Date().toLocaleTimeString().replace(/^\D*/, '')

      if (this.machineChange) {
        this.machineChange = false
        this.categories = []
        this.categories2 = []
        this.data1 = []
        this.data2 = []
        this.data3 = []
        this.data4 = []
        // debugger
        let formData = new FormData()
        formData.set('param1', this.machineNum)
        this.httpClient
          .post<any>(environment.apiServerURL + 'Content/QueryAll', formData)
          .subscribe({
            next: (res) => {
              // debugger
              res.forEach((element) => {
                this.categories.push(
                  moment(element.upd_time.substring(0, 19)).format('HH:mm'),
                )
                this.categories2.push(
                  moment(element.upd_time.substring(0, 19)).format('MM-DD'),
                )
                this.data1.push(element.cool_input)
                this.data2.push(element.cool_output)
                this.data3.push(element.cool_down_input)
                this.data4.push(element.cool_down_output)
              })
              this.latestDataTime = res[res.length - 1].upd_time
              this.myChartGlobal1.setOption<echarts.EChartsOption>({
                xAxis: [
                  {
                    data: this.categories,
                  },
                  {
                    data: this.categories2,
                  },
                ],
                series: [
                  {
                    data: this.data1,
                  },
                  {
                    data: this.data1,
                  },
                ],
              })
              this.myChartGlobal2.setOption<echarts.EChartsOption>({
                xAxis: [
                  {
                    data: this.categories,
                  },
                  {
                    data: this.categories2,
                  },
                ],
                series: [
                  {
                    data: this.data2,
                  },
                  {
                    data: this.data2,
                  },
                ],
              })
              this.myChartGlobal3.setOption<echarts.EChartsOption>({
                xAxis: [
                  {
                    data: this.categories,
                  },
                  {
                    data: this.categories2,
                  },
                ],
                series: [
                  {
                    data: this.data3,
                  },
                  {
                    data: this.data3,
                  },
                ],
              })
              this.myChartGlobal4.setOption<echarts.EChartsOption>({
                xAxis: [
                  {
                    data: this.categories,
                  },
                  {
                    data: this.categories2,
                  },
                ],
                series: [
                  {
                    data: this.data4,
                  },
                  {
                    data: this.data4,
                  },
                ],
              })
            },
            error: (err) => {
              console.error(err)
              this.toastr.error('request failed')
            },
          })
      } else if (this.onInitFlag) {
        this.onInitFlag = false
        myChart.setOption<echarts.EChartsOption>({
          xAxis: [
            {
              data: this.categories,
            },
            {
              data: this.categories2,
            },
          ],
          series: [
            {
              data: this.data1,
            },
            {
              data: this.data1,
            },
          ],
        })
        myChart2.setOption<echarts.EChartsOption>({
          xAxis: [
            {
              data: this.categories,
            },
            {
              data: this.categories2,
            },
          ],
          series: [
            {
              data: this.data2,
            },
            {
              data: this.data2,
            },
          ],
        })
        myChart3.setOption<echarts.EChartsOption>({
          xAxis: [
            {
              data: this.categories,
            },
            {
              data: this.categories2,
            },
          ],
          series: [
            {
              data: this.data3,
            },
            {
              data: this.data3,
            },
          ],
        })
        myChart4.setOption<echarts.EChartsOption>({
          xAxis: [
            {
              data: this.categories,
            },
            {
              data: this.categories2,
            },
          ],
          series: [
            {
              data: this.data4,
            },
            {
              data: this.data4,
            },
          ],
        })
      } else {
        let formData = new FormData()
        formData.set('param1', this.machineNum)
        this.httpClient
          .post<any>(
            environment.apiServerURL + 'Content/QueryLatestOne',
            formData,
          )
          .subscribe({
            next: (res) => {
              // debugger
              if (this.latestDataTime != res.upd_time) {
                this.latestDataTime = res.upd_time
                this.categories.shift()
                this.categories.push(
                  moment(res.upd_time.substring(0, 19)).format('HH:mm'),
                )
                this.categories2.shift()
                this.categories2.push(
                  moment(res.upd_time.substring(0, 19)).format('MM-DD'),
                )

                this.data1.shift()
                this.data1.push(res.cool_input)
                this.data2.shift()
                this.data2.push(res.cool_output)
                this.data3.shift()
                this.data3.push(res.cool_down_input)
                this.data4.shift()
                this.data4.push(res.cool_down_output)
                myChart.setOption<echarts.EChartsOption>({
                  xAxis: [
                    {
                      data: this.categories,
                    },
                    {
                      data: this.categories2,
                    },
                  ],
                  series: [
                    {
                      data: this.data1,
                    },
                    {
                      data: this.data1,
                    },
                  ],
                })
                myChart2.setOption<echarts.EChartsOption>({
                  xAxis: [
                    {
                      data: this.categories,
                    },
                    {
                      data: this.categories2,
                    },
                  ],
                  series: [
                    {
                      data: this.data2,
                    },
                    {
                      data: this.data2,
                    },
                  ],
                })
                myChart3.setOption<echarts.EChartsOption>({
                  xAxis: [
                    {
                      data: this.categories,
                    },
                    {
                      data: this.categories2,
                    },
                  ],
                  series: [
                    {
                      data: this.data3,
                    },
                    {
                      data: this.data3,
                    },
                  ],
                })
                myChart4.setOption<echarts.EChartsOption>({
                  xAxis: [
                    {
                      data: this.categories,
                    },
                    {
                      data: this.categories2,
                    },
                  ],
                  series: [
                    {
                      data: this.data4,
                    },
                    {
                      data: this.data4,
                    },
                  ],
                })
              }
            },
            error: (err) => {
              console.error(err)
              this.toastr.error('request failed')
            },
          })
      }
      // data.shift()
      // data.push(Math.round(Math.random() * 1000))

      // data2.shift()
      // data2.push(+(Math.random() * 10 + 5).toFixed(1))

      // categories.shift()
      // categories.push(axisData)
      // categories2.shift()
      // categories2.push("")

      // if (count.length <= 0) {
      //   count.push(1)
      // } else {
      //   const value = sessionStorage.getItem('tt')
      //   var obj = JSON.parse(value)
      //   clearInterval(obj)
      // }
    }, 2000)
    // var tt = setInterval(function () {

    // }, 2000)

    // sessionStorage.setItem('tt', JSON.stringify(tt))
    this.chart1 = chartDom
    this.chart2 = chartDom2
    this.chart3 = chartDom3
    this.chart4 = chartDom4
    option && myChart.setOption(option)
    option2 && myChart2.setOption(option2)
    option3 && myChart3.setOption(option3)
    option4 && myChart4.setOption(option4)
    //#endregion

    this.myChartGlobal1 = myChart
    this.myChartGlobal2 = myChart2
    this.myChartGlobal3 = myChart3
    this.myChartGlobal4 = myChart4

    this.optionGlobal1 = option
    this.optionGlobal2 = option2
    this.optionGlobal3 = option3
    this.optionGlobal4 = option4

    this.categoriesGlobal1 = this.categories
    this.categoriesGlobal2 = this.categories2
    this.dataGlobal1 = this.data1
  }

  Visible(data: any, Type: string) {
    console.log(data)

    if (Type == 'List') this.isShowVector = !data
    else this.isShowUserVector = !data
  }

  selectDiv(item: number): void {
    switch (item) {
      case 1:
        this.selectedDiv = '冰水出水'
        this.chart1.style.width = '100' + '%'
        this.chart1.style.height = '800' + '%'
        this.myChartGlobal1.resize()
        // this.myChartGlobal1.clear()
        // this.myChartGlobal1.resize({
        //   width: '1200%',
        //   height: '600%',
        // })

        // this.myChartGlobal1.setOption(this.optionGlobal1)
        // this.myChartGlobal1.resize()
        break
      case 2:
        this.selectedDiv = '冰水入水'
        this.chart2.style.width = '100' + '%'
        this.chart2.style.height = '800' + '%'
        this.myChartGlobal2.resize()
        break
      case 3:
        this.selectedDiv = '冷卻水出水'
        this.chart3.style.width = '100' + '%'
        this.chart3.style.height = '800' + '%'
        this.myChartGlobal3.resize()
        break
      case 4:
        this.selectedDiv = '冷卻水入水'
        this.chart4.style.width = '100' + '%'
        this.chart4.style.height = '800' + '%'
        this.myChartGlobal4.resize()
        break
    }
  }

  clearSelected(): void {
    if (this.selectedDiv == '冰水出水') {
      this.chart1.style.width = '50' + '%'
      this.chart1.style.height = '400' + '%'
      this.myChartGlobal1.resize()
    } else if (this.selectedDiv == '冰水入水') {
      this.chart2.style.width = '50' + '%'
      this.chart2.style.height = '400' + '%'
      this.myChartGlobal2.resize()
    } else if (this.selectedDiv == '冷卻水出水') {
      this.chart3.style.width = '50' + '%'
      this.chart3.style.height = '400' + '%'
      this.myChartGlobal3.resize()
    } else if (this.selectedDiv == '冷卻水入水') {
      this.chart4.style.width = '50' + '%'
      this.chart4.style.height = '400' + '%'
      this.myChartGlobal4.resize()
    }

    // this.chart2.style.width = '50' + '%'
    // this.chart2.style.height = '400' + '%'
    // this.myChartGlobal2.resize()
    // this.chart3.style.width = '50' + '%'
    // this.chart3.style.height = '400' + '%'
    // this.myChartGlobal3.resize()
    // this.chart4.style.width = '50' + '%'
    // this.chart4.style.height = '400' + '%'
    // this.myChartGlobal4.resize()
    this.selectedDiv = 'initial'
  }
}
