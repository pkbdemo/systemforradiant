import { Component, OnInit } from '@angular/core'
import { LayoutService } from '../layout/services/layout.service'
import { Router } from '@angular/router'
import { IMSConstants } from 'src/app/utils/IMSConstants'

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
})
export class IndexComponent implements OnInit {
  contactEmail = IMSConstants.contactEmail

  constructor(private layoutService: LayoutService, private router: Router) {}

  ngOnInit() {
    this.router.navigate(['/realtimetext'], {})
  }

  toggleRightBar() {
    this.layoutService.toggleRightBar()
  }
}
