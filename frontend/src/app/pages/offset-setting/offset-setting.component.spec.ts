import { ComponentFixture, TestBed } from '@angular/core/testing'

import { OffsetSettingViewComponent } from './offset-setting.component'

describe('OffsetSettingViewComponent', () => {
  let component: OffsetSettingViewComponent
  let fixture: ComponentFixture<OffsetSettingViewComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OffsetSettingViewComponent],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(OffsetSettingViewComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
