import { Component, OnInit } from '@angular/core'
import { LayoutService } from '../layout/services/layout.service'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'
import { environment } from '../../../environments/environment'
import { NzButtonSize } from 'ng-zorro-antd/button'
import { NzMessageService } from 'ng-zorro-antd/message'
import { Router } from '@angular/router'

@Component({
  selector: 'app-offset-setting',
  templateUrl: './offset-setting.component.html',
  styleUrls: ['./offset-setting.component.css'],
})
export class OffsetSettingViewComponent implements OnInit {
  startValue: Date | null = null
  endValue: Date | null = null

  userName: string
  userId: string
  size: NzButtonSize = 'large'

  //鍵盤相關
  offset1_1 = ''
  offset1_2 = ''
  offset1_3 = ''
  offset1_4 = ''
  offset2_1 = ''
  offset2_2 = ''
  offset2_3 = ''
  offset2_4 = ''
  offset3_1 = ''
  offset3_2 = ''
  offset3_3 = ''
  offset3_4 = ''

  constructor(
    private layoutService: LayoutService,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private message: NzMessageService,
    private router: Router,
  ) {}

  async ngOnInit() {
    let formData = new FormData()
    formData.set('param1', '')
    this.httpClient
      .post<any>(environment.apiServerURL + 'Setting/QueryOffset', formData)
      .subscribe({
        next: (res) => {
          this.offset1_1 = res.offset1_1
          this.offset1_2 = res.offset1_2
          this.offset1_3 = res.offset1_3
          this.offset1_4 = res.offset1_4
          this.offset2_1 = res.offset2_1
          this.offset2_2 = res.offset2_2
          this.offset2_3 = res.offset2_3
          this.offset2_4 = res.offset2_4
          this.offset3_1 = res.offset3_1
          this.offset3_2 = res.offset3_2
          this.offset3_3 = res.offset3_3
          this.offset3_4 = res.offset3_4
        },
        error: (err) => {
          console.error(err)
          this.toastr.error('request failed')
        },
      })
  }

  update() {
    let formData = new FormData()

    formData.set('Offset1_1', this.offset1_1)
    formData.set('Offset1_2', this.offset1_2)
    formData.set('Offset1_3', this.offset1_3)
    formData.set('Offset1_4', this.offset1_4)
    formData.set('Offset2_1', this.offset2_1)
    formData.set('Offset2_2', this.offset2_2)
    formData.set('Offset2_3', this.offset2_3)
    formData.set('Offset2_4', this.offset2_4)
    formData.set('Offset3_1', this.offset3_1)
    formData.set('Offset3_2', this.offset3_2)
    formData.set('Offset3_3', this.offset3_3)
    formData.set('Offset3_4', this.offset3_4)
    this.httpClient
      .post<any>(environment.apiServerURL + 'Setting/UpdateOffset', formData)
      .subscribe({
        next: (res) => {
          if (res) {
            this.router.navigate(['offsetsetting'])
          }
        },
        error: (err) => {
          console.error(err)
          this.toastr.error('request failed')
        },
      })
    this.createMessage('success')
  }

  createMessage(type: string): void {
    if (type == 'success') this.message.create(type, '儲存成功')
    if (type == 'error') this.message.create(type, '儲存失敗，格式不符')
  }
}
