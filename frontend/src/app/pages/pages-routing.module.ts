import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { AdminComponent } from './layout/components/admin/admin.component'

import { BasicformComponent } from './examples/basicform/basicform.component'
import { AdvancedformComponent } from './examples/advancedform/advancedform.component'
import { FormexamplesComponent } from './examples/formexamples/formexamples.component'
import { FormvalidationComponent } from './examples/formvalidation/formvalidation.component'
import { SummernoteComponent } from './examples/summernote/summernote.component'
import { CallapiComponent } from './examples/callapi/callapi.component'
import { GridviewComponent } from './examples/gridview/gridview.component'
import { IndexComponent } from './index/index.component'
import { DueDiligenceComponent } from './investigation/duediligence/due-diligence/due-diligence.component'
import { RealTimeViewComponent } from './real-time/real-time-view.component'
import { RealTimeTextViewComponent } from './real-time-text/real-time-text.component'
import { RealTimeTextYesterdayViewComponent } from './real-time-text-yesterday/real-time-text-yesterday.component'
import { RealTimeTextWeekViewComponent } from './real-time-text-week/real-time-text-week.component'
import { RealTimeTextMonthViewComponent } from './real-time-text-month/real-time-text-month.component'
import { ParameterSettingViewComponent } from './parameter-setting/parameter-setting.component'
import { OffsetSettingViewComponent } from './offset-setting/offset-setting.component'
import { SummaryTableMonthViewComponent } from './summary-table-month/summary-table-month.component'
import { SummaryTableDayViewComponent } from './summary-table-day/summary-table-day.component'
import { SummaryTableHourViewComponent } from './summary-table-hour/summary-table-hour.component'
const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: '', redirectTo: 'index', pathMatch: 'full' },

      // Home
      { path: 'index', component: IndexComponent },

      // Examples
      { path: 'basicform', component: BasicformComponent },
      { path: 'advancedform', component: AdvancedformComponent },
      { path: 'formexamples', component: FormexamplesComponent },
      { path: 'formvalidation', component: FormvalidationComponent },
      { path: 'formsummernote', component: SummernoteComponent },
      { path: 'callapi', component: CallapiComponent },
      { path: 'gridview', component: GridviewComponent },
      { path: 'duediligence', component: DueDiligenceComponent },
      { path: 'realtime', component: RealTimeViewComponent },
      { path: 'parametersetting', component: ParameterSettingViewComponent },
      { path: 'realtimetext', component: RealTimeTextViewComponent },
      {
        path: 'realtimetextyesterday',
        component: RealTimeTextYesterdayViewComponent,
      },
      {
        path: 'realtimetextweek',
        component: RealTimeTextWeekViewComponent,
      },
      {
        path: 'realtimetextmonth',
        component: RealTimeTextMonthViewComponent,
      },
      {
        path: 'summarytablemonth',
        component: SummaryTableMonthViewComponent,
      },
      {
        path: 'summarytableday',
        component: SummaryTableDayViewComponent,
      },
      {
        path: 'summarytablehour',
        component: SummaryTableHourViewComponent,
      },
      {
        path: 'offsetsetting',
        component: OffsetSettingViewComponent,
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
