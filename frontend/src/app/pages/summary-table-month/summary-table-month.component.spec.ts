import { ComponentFixture, TestBed } from '@angular/core/testing'

import { SummaryTableMonthViewComponent } from './summary-table-month.component'

describe('SummaryTableMonthViewComponent', () => {
  let component: SummaryTableMonthViewComponent
  let fixture: ComponentFixture<SummaryTableMonthViewComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SummaryTableMonthViewComponent],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryTableMonthViewComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
