import { ComponentFixture, TestBed } from '@angular/core/testing'

import { RealTimeTextMonthViewComponent } from './real-time-text-month.component'

describe('RealTimeTextMonthViewComponent', () => {
  let component: RealTimeTextMonthViewComponent
  let fixture: ComponentFixture<RealTimeTextMonthViewComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RealTimeTextMonthViewComponent],
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(RealTimeTextMonthViewComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
