import { Component, OnInit } from '@angular/core'
import { LayoutService } from '../layout/services/layout.service'
import { HttpClient } from '@angular/common/http'
import { ToastrService } from 'ngx-toastr'
import { environment } from '../../../environments/environment'
import { dropdownItem } from '../real-time-text/parameterSettingModel'
import { NzButtonSize } from 'ng-zorro-antd/button'
import Keyboard from 'simple-keyboard'
import { NzMessageService } from 'ng-zorro-antd/message'
import { Router } from '@angular/router'
import moment from 'moment'
interface navBtnItem {
  Code_id: string
  Code_Name: string
}
@Component({
  selector: 'app-real-time-text-month',
  templateUrl: './real-time-text-month.component.html',
  styleUrls: ['./real-time-text-month.component.css'],
})
export class RealTimeTextMonthViewComponent implements OnInit {
  interval: any
  machineNum: string = '1'
  cold_input: number = 0
  cold_output: number = 0
  cool_down_input: number = 0
  cool_down_output: number = 0
  latestDataTime: string = ''
  error_temp1 = ''
  error_temp2 = ''
  error_temp3 = ''
  error_down1 = ''
  error_down2 = ''
  error_down3 = ''
  constructor(
    private layoutService: LayoutService,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private message: NzMessageService,
    private router: Router,
  ) {}
  async ngOnInit() {
    let formData = new FormData()
    formData.set('param1', this.machineNum)
    this.httpClient
      .post<any>(
        environment.apiServerURL + 'Content/ReadLastMonthAvg',
        formData,
      )
      .subscribe({
        next: (res) => {
          if (this.latestDataTime != res.upd_time) {
            this.cold_input = 0
            this.cold_output = 0
            this.cool_down_input = 0
            this.cool_down_output = 0
            this.latestDataTime = res.upd_time
            this.cold_input = res.cool_input
            this.cold_output = res.cool_output
            this.cool_down_input = res.cool_down_input
            this.cool_down_output = res.cool_down_output
          }
        },
        error: (err) => {
          console.error(err)
          this.toastr.error('request failed')
        },
      })

    let formData3 = new FormData()
    formData3.set('param1', '')
    this.httpClient
      .post<any>(environment.apiServerURL + 'Setting/Query', formData3)
      .subscribe({
        next: (res) => {
          this.error_temp1 = res.error_Temp1
          this.error_temp2 = res.error_Temp2
          this.error_temp3 = res.error_Temp3
          this.error_down1 = res.error_Down1
          this.error_down2 = res.error_Down2
          this.error_down3 = res.error_Down3
        },
        error: (err) => {
          console.error(err)
          this.toastr.error('request failed')
        },
      })
  }

  changeMachineNum() {
    let formData = new FormData()
    formData.set('param1', this.machineNum)
    this.httpClient
      .post<any>(
        environment.apiServerURL + 'Content/ReadLastMonthAvg',
        formData,
      )
      .subscribe({
        next: (res) => {
          this.cold_input = 0
          this.cold_output = 0
          this.cool_down_input = 0
          this.cool_down_output = 0
          this.latestDataTime = res.upd_time
          this.cold_input = res.cool_input
          this.cold_output = res.cool_output
          this.cool_down_input = res.cool_down_input
          this.cool_down_output = res.cool_down_output
        },
        error: (err) => {
          console.error(err)
          this.toastr.error('request failed')
        },
      })
  }

  createMessage(type: string): void {
    if (type == 'success') this.message.create(type, '儲存成功')
    if (type == 'error') this.message.create(type, '儲存失敗，格式不符')
  }
}
