import { Component, OnInit } from '@angular/core'
import { NavigationEnd, Router } from '@angular/router'
import { KeycloakService } from 'keycloak-angular'
import { filter } from 'rxjs'
import { HttpClient } from '@angular/common/http'
import { IMSConstants } from 'src/app/utils/IMSConstants'
import { environment } from 'src/environments/environment'
import { ToastrService } from 'ngx-toastr'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'IMS'
  timer: any
  constructor(
    private router: Router,
    private httpClient: HttpClient,
    private toastr: ToastrService,
  ) {}

  ngOnInit(): void {
    var temp = sessionStorage.getItem('intervalItems')
    if (temp == null) {
      let formData2 = new FormData()
      formData2.set('param1', '')
      this.httpClient
        .post<any>(environment.apiServerURL + 'Setting/Query', formData2)
        .subscribe({
          next: (res) => {
            debugger
            console.log(res)
            this.timer = res.timer
            var interval = setInterval(() => {
              this.httpClient
                .get<any>(environment.apiServerURL + 'Setting/TriggerJob')
                .subscribe({
                  next: (res) => {
                    debugger
                    console.log(res)
                  },
                  error: (err) => {
                    console.error(err)
                    this.toastr.error('request failed')
                  },
                })
            }, res.timer * 1000 * 60)
            sessionStorage.setItem(
              'intervalItems',
              JSON.stringify({
                item: interval,
              }),
            )
          },
          error: (err) => {
            console.error(err)
            this.toastr.error('request failed')
          },
        })
    }
  }
}
