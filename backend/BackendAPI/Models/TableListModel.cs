﻿namespace BackendAPI.Models;

public class WaterTemp
{
    public decimal Cool_input { get; set; }
    public decimal Cool_output { get; set; }
    public decimal Cool_down_input { get; set; }
    public decimal Cool_down_output { get; set; }
    public string? Upd_time { get; set; }
    public int? Machine_num { get; set; }
}

public class AllWaterTemp
{
    public decimal Cool_input1 { get; set; }
    public decimal Cool_output1 { get; set; }
    public decimal Cool_down_input1 { get; set; }
    public decimal Cool_down_output1 { get; set; }

    public decimal Cool_input2 { get; set; }
    public decimal Cool_output2 { get; set; }
    public decimal Cool_down_input2 { get; set; }
    public decimal Cool_down_output2 { get; set; }

    public decimal Cool_input3 { get; set; }
    public decimal Cool_output3 { get; set; }
    public decimal Cool_down_input3 { get; set; }
    public decimal Cool_down_output3 { get; set; }

    public string Status_flag1 { get; set; }
    public string Status_flag2 { get; set; }
    public string Status_flag3 { get; set; }

    public string? Upd_time { get; set; }
    public int? Machine_num { get; set; }
}