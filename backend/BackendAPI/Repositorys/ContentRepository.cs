﻿namespace BackendAPI.Repositorys;

public class ContentRepository : IContentRepository
{
    private IDBHelper _dbHelper;

    public ContentRepository(IDBHelper dbHelper)
    {
        this._dbHelper = dbHelper;
    }

    public IList<EChartEntity> QueryAll(string? param1)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "select * from (select * from water_temp where machine_num = " + param1 + " order by upd_time desc limit 30 ) where status_flag = 'T' order by upd_time asc ";
        var bASExampleEntity = dbConn.Query<EChartEntity>(sql).ToList();
        return bASExampleEntity;
    }

    public EChartEntity QueryLatestOne(string? param1)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "select * from (select * from immediate_temp where machine_num = " + param1 + " order by upd_time desc limit 1 ) order by upd_time asc ";
        var bASExampleEntity = dbConn.QueryFirstOrDefault<EChartEntity>(sql);
        return bASExampleEntity;
    }

    public IList<EChartEntity> ReadLastAvg(string? param1)
    {
        DateTime today = DateTime.Today;
        DateTime previousDay = today.AddDays(-1);
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @" SELECT * FROM water_temp WHERE status_flag = 'T' and upd_time BETWEEN datetime('" + previousDay.ToString("u") + "') AND datetime('" + today.ToString("u") + "') and machine_num =" + param1.ToString();
        var bASExampleEntity = dbConn.Query<EChartEntity>(sql).ToList();
        return bASExampleEntity;
    }
    public IList<EChartEntity> ReadLastWeekAvg(string? param1)
    {
        DayOfWeek weekStart = DayOfWeek.Monday;
        DateTime startingDate = DateTime.Today;
        while (startingDate.DayOfWeek != weekStart)
            startingDate = startingDate.AddDays(-1);
        DateTime previousWeekStart = startingDate.AddDays(-7);
        DateTime previousWeekEnd = startingDate.AddDays(-1);

        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @" SELECT * FROM water_temp WHERE status_flag = 'T' and upd_time BETWEEN datetime('" + previousWeekStart.ToString("u") + "') AND datetime('" + previousWeekEnd.ToString("u") + "') and machine_num =" + param1.ToString();
        var bASExampleEntity = dbConn.Query<EChartEntity>(sql).ToList();
        return bASExampleEntity;
    }
    public IList<EChartEntity> ReadLastMonthAvg(string? param1)
    {
        DateTime previousDate = DateTime.Now.AddMonths(-1);
        var startDate = new DateTime(previousDate.Year, previousDate.Month, 1);
        var endDate = startDate.AddMonths(1).AddDays(-1);

        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @" SELECT * FROM water_temp WHERE status_flag = 'T' and upd_time BETWEEN datetime('" + startDate.ToString("u") + "') AND datetime('" + endDate.ToString("u") + "') and machine_num =" + param1.ToString();
        var bASExampleEntity = dbConn.Query<EChartEntity>(sql).ToList();
        return bASExampleEntity;
    }
    public IList<ReportEntity> QueryDailayReport(string? machine_num, string? startDate, string? endDate)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "";
        if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate))
            sql = "select * from ( SELECT date(upd_time) upd_time, ROUND(AVG(cool_input),1)cool_input, ROUND(AVG(cool_output),1) cool_output, ABS(ROUND(AVG(cool_input) - AVG(cool_output),1)) cool_temp_diff , ROUND(AVG(cool_down_input),1) cool_down_input,  ROUND(AVG(cool_down_output),1)cool_down_output,  ABS(ROUND(AVG(cool_down_input) - AVG(cool_down_output),1)) cool_down_temp_diff FROM water_temp where status_flag = 'T' and machine_num = " + machine_num + " GROUP BY  date(upd_time)) order by  upd_time desc LIMIT 500";
        else
            sql = "select * from ( SELECT date(upd_time) upd_time, ROUND(AVG(cool_input),1)cool_input, ROUND(AVG(cool_output),1) cool_output, ABS(ROUND(AVG(cool_input) - AVG(cool_output),1)) cool_temp_diff , ROUND(AVG(cool_down_input),1) cool_down_input,  ROUND(AVG(cool_down_output),1)cool_down_output,  ABS(ROUND(AVG(cool_down_input) - AVG(cool_down_output),1)) cool_down_temp_diff FROM water_temp where status_flag = 'T' and machine_num = " + machine_num + " GROUP BY  date(upd_time)) where upd_time BETWEEN '" + startDate + "' and '" + endDate + "' order by  upd_time desc";
        var bASExampleEntity = dbConn.Query<ReportEntity>(sql).ToList();
        return bASExampleEntity;
    }
    public IList<ReportEntity> QueryMonthlyReport(string? machine_num, string? startDate, string? endDate)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "";
        if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate))
            sql = "select * from (SELECT strftime('%Y-%m', upd_time) upd_time, ROUND( AVG(cool_input),1) cool_input,ROUND(AVG(cool_output),1)cool_output, ABS(ROUND(AVG(cool_input) - AVG(cool_output),1)) cool_temp_diff , ROUND(AVG(cool_down_input),1)cool_down_input,  ROUND(AVG(cool_down_output),1) cool_down_output, ABS(ROUND(AVG(cool_down_input) - AVG(cool_down_output),1)) cool_down_temp_diff FROM water_temp where status_flag = 'T' and machine_num = " + machine_num + " GROUP BY  strftime('%Y-%m', upd_time))  order by upd_time desc  LIMIT 500 ";
        else
            sql = "select * from (SELECT strftime('%Y-%m', upd_time) upd_time, ROUND( AVG(cool_input),1) cool_input,ROUND(AVG(cool_output),1)cool_output, ABS(ROUND(AVG(cool_input) - AVG(cool_output),1)) cool_temp_diff , ROUND(AVG(cool_down_input),1)cool_down_input,  ROUND(AVG(cool_down_output),1) cool_down_output, ABS(ROUND(AVG(cool_down_input) - AVG(cool_down_output),1)) cool_down_temp_diff FROM water_temp where status_flag = 'T' and machine_num = " + machine_num + " GROUP BY  strftime('%Y-%m', upd_time))where upd_time BETWEEN '" + startDate + "' and '" + endDate + "' order by  upd_time desc";
        var bASExampleEntity = dbConn.Query<ReportEntity>(sql).ToList();
        return bASExampleEntity;
    }
    public IList<ReportEntity> QueryHourlyReport(string? machine_num, string? startDate, string? endDate)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "";
        if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate))
            sql = "select * from (SELECT strftime('%Y-%m-%d %H', upd_time) upd_time, ROUND(AVG(cool_input),1) cool_input,ROUND(AVG(cool_output),1)cool_output, ABS(ROUND(AVG(cool_input) - AVG(cool_output),1)) cool_temp_diff , ROUND(AVG(cool_down_input),1)cool_down_input,  ROUND(AVG(cool_down_output),1) cool_down_output, ABS(ROUND( AVG(cool_down_input) - AVG(cool_down_output),1)) cool_down_temp_diff FROM water_temp where status_flag = 'T' and machine_num =" + machine_num + " GROUP BY  strftime('%Y-%m-%d %H', upd_time)) order by  upd_time desc  LIMIT 500 ";//2022-08-08 20
        else
            sql = "select * from (SELECT strftime('%Y-%m-%d %H', upd_time) upd_time, ROUND(AVG(cool_input),1) cool_input,ROUND(AVG(cool_output),1)cool_output, ABS(ROUND(AVG(cool_input) - AVG(cool_output),1)) cool_temp_diff , ROUND(AVG(cool_down_input),1)cool_down_input,  ROUND(AVG(cool_down_output),1) cool_down_output, ABS(ROUND( AVG(cool_down_input) - AVG(cool_down_output),1)) cool_down_temp_diff FROM water_temp where status_flag = 'T' and machine_num =" + machine_num + " GROUP BY  strftime('%Y-%m-%d %H', upd_time))where upd_time BETWEEN '" + startDate + "' and '" + endDate + "' order by  upd_time desc";
        var bASExampleEntity = dbConn.Query<ReportEntity>(sql).ToList();
        return bASExampleEntity;
    }

    public IList<ReportCoolOutputEntity> QueryCoolOutputReport(string? machine_num, string? startDate, string? endDate)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "";
        if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate))
            sql = "select  strftime('%Y-%m-%d %H:%M', upd_time) upd_time, cool_output  from water_temp where status_flag = 'T' and machine_num = " + machine_num + " order by  upd_time desc LIMIT 500 ";//2022-08-08 20
        else
            sql = "select  strftime('%Y-%m-%d %H:%M', upd_time) upd_time, cool_output  from water_temp where status_flag = 'T' and machine_num =" + machine_num + " and upd_time BETWEEN '" + startDate + "' and '" + endDate + "' order by  upd_time desc";
        var bASExampleEntity = dbConn.Query<ReportCoolOutputEntity>(sql).ToList();
        return bASExampleEntity;
    }
    public IList<ReportCoolInputEntity> QueryCoolInputReport(string? machine_num, string? startDate, string? endDate)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "";
        if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate))
            sql = "select  strftime('%Y-%m-%d %H:%M', upd_time) upd_time, cool_input  from water_temp where status_flag = 'T' and machine_num = " + machine_num + " order by  upd_time desc LIMIT 500 ";//2022-08-08 20
        else
            sql = "select  strftime('%Y-%m-%d %H:%M', upd_time) upd_time, cool_input  from water_temp where status_flag = 'T' and machine_num =" + machine_num + " and upd_time BETWEEN '" + startDate + "' and '" + endDate + "' order by  upd_time desc";
        var bASExampleEntity = dbConn.Query<ReportCoolInputEntity>(sql).ToList();
        return bASExampleEntity;
    }
    public IList<ReportCoolDownOutputEntity> QueryCoolDownOutputReport(string? machine_num, string? startDate, string? endDate)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "";
        if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate))
            sql = "select  strftime('%Y-%m-%d %H:%M', upd_time) upd_time, cool_down_output  from water_temp where status_flag = 'T' and machine_num = " + machine_num + " order by  upd_time desc LIMIT 500 ";//2022-08-08 20
        else
            sql = "select  strftime('%Y-%m-%d %H:%M', upd_time) upd_time, cool_down_output  from water_temp where status_flag = 'T' and machine_num =" + machine_num + " and upd_time BETWEEN '" + startDate + "' and '" + endDate + "' order by  upd_time desc";
        var bASExampleEntity = dbConn.Query<ReportCoolDownOutputEntity>(sql).ToList();
        return bASExampleEntity;
    }
    public IList<ReportCoolDownInputEntity> QueryCoolDownInputReport(string? machine_num, string? startDate, string? endDate)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "";
        if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate))
            sql = "select  strftime('%Y-%m-%d %H:%M', upd_time) upd_time, cool_down_input  from water_temp where status_flag = 'T' and machine_num = " + machine_num + " order by  upd_time desc LIMIT 500 ";//2022-08-08 20
        else
            sql = "select  strftime('%Y-%m-%d %H:%M', upd_time) upd_time, cool_down_input  from water_temp where status_flag = 'T' and machine_num =" + machine_num + " and upd_time BETWEEN '" + startDate + "' and '" + endDate + "' order by  upd_time desc";
        var bASExampleEntity = dbConn.Query<ReportCoolDownInputEntity>(sql).ToList();
        return bASExampleEntity;
    }
    public IList<ReportCoolDiffEntity> QueryCoolDiffReport(string? machine_num, string? startDate, string? endDate)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "";
        if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate))
            sql = "select  strftime('%Y-%m-%d %H:%M', upd_time) upd_time, ABS(ROUND(cool_input - cool_output,1)) cool_diff from water_temp where status_flag = 'T' and machine_num = " + machine_num + " order by  upd_time desc LIMIT 500 ";//2022-08-08 20
        else
            sql = "select  strftime('%Y-%m-%d %H:%M', upd_time) upd_time, ABS(ROUND(cool_input - cool_output,1)) cool_diff from water_temp where status_flag = 'T' and machine_num =" + machine_num + " and upd_time BETWEEN '" + startDate + "' and '" + endDate + "' order by  upd_time desc";
        var bASExampleEntity = dbConn.Query<ReportCoolDiffEntity>(sql).ToList();
        return bASExampleEntity;
    }
    public IList<ReportCoolDownDiffEntity> QueryCoolDownDiffReport(string? machine_num, string? startDate, string? endDate)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "";
        if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate))
            sql = "select  strftime('%Y-%m-%d %H:%M', upd_time) upd_time, ABS(ROUND(cool_down_input - cool_down_output,1)) cool_down_diff from water_temp where status_flag = 'T' and machine_num = " + machine_num + " order by  upd_time desc LIMIT 500 ";//2022-08-08 20
        else
            sql = "select  strftime('%Y-%m-%d %H:%M', upd_time) upd_time, ABS(ROUND(cool_down_input - cool_down_output,1)) cool_down_diff from water_temp where status_flag = 'T' and machine_num =" + machine_num + " and upd_time BETWEEN '" + startDate + "' and '" + endDate + "' order by  upd_time desc";
        var bASExampleEntity = dbConn.Query<ReportCoolDownDiffEntity>(sql).ToList();
        return bASExampleEntity;
    }
}