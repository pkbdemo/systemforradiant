﻿namespace BackendAPI.Repositorys.IRepositorys;

public interface IContentRepository
{
    IList<EChartEntity> QueryAll(string? param1);
    EChartEntity QueryLatestOne(string? param1);
    IList<EChartEntity> ReadLastAvg(string? param1);
    IList<EChartEntity> ReadLastWeekAvg(string? param1);
    IList<EChartEntity> ReadLastMonthAvg(string? param1);
    IList<ReportEntity> QueryDailayReport(string? machine_num, string? startDate, string? endDate);
    IList<ReportEntity> QueryMonthlyReport(string? machine_num, string? startDate, string? endDate);
    IList<ReportEntity> QueryHourlyReport(string? param1, string? startDate, string? endDate);
    IList<ReportCoolOutputEntity> QueryCoolOutputReport(string? param1, string? startDate, string? endDate);
    IList<ReportCoolInputEntity> QueryCoolInputReport(string? param1, string? startDate, string? endDate);
    IList<ReportCoolDownOutputEntity> QueryCoolDownOutputReport(string? param1, string? startDate, string? endDate);
    IList<ReportCoolDownInputEntity> QueryCoolDownInputReport(string? param1, string? startDate, string? endDate);
    IList<ReportCoolDiffEntity> QueryCoolDiffReport(string? param1, string? startDate, string? endDate);
    IList<ReportCoolDownDiffEntity> QueryCoolDownDiffReport(string? param1, string? startDate, string? endDate);
}