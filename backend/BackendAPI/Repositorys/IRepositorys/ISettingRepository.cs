﻿namespace BackendAPI.Repositorys.IRepositorys;

public interface ISettingRepository
{
    OffsetEntity QueryOffset();
    BasicSettingEntity QueryAll();
    GateSettingEntity QueryGate();
    int AddData(AllWaterTemp temp);
    int AddImmediateData(AllWaterTemp temp);
    int DeleteImmediateData();
    int Update(BasicSettingEntity request);
    int UpdateOffset(OffsetEntity request);
    int UpdateGate1(string request);
    int UpdateGate2(string request);
    int UpdateGate3(string request);
}