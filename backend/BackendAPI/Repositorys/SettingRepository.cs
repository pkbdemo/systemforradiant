﻿namespace BackendAPI.Repositorys;

public class SettingRepository : ISettingRepository
{
    private IDBHelper _dbHelper;

    public SettingRepository(IDBHelper dbHelper)
    {
        this._dbHelper = dbHelper;
    }
    public OffsetEntity QueryOffset()
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "select * from offset_Setting";
        var bASExampleEntity = dbConn.QueryFirstOrDefault<OffsetEntity>(sql);
        return bASExampleEntity;
    }
    public BasicSettingEntity QueryAll()
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "select * from basic_Setting";
        var bASExampleEntity = dbConn.QueryFirstOrDefault<BasicSettingEntity>(sql);
        return bASExampleEntity;
    }

    public GateSettingEntity QueryGate()
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = "select * from gate_Setting";
        var bASExampleEntity = dbConn.QueryFirstOrDefault<GateSettingEntity>(sql);
        return bASExampleEntity;
    }

    public int AddData(AllWaterTemp param)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        //param.Status_flag1
        //param.Status_flag2
        //param.Status_flag3
        string sql = @"
        INSERT INTO water_temp
        SELECT '" + param.Cool_input1.ToString() + "' AS 'cool_input', '" + param.Cool_output1.ToString() + "' AS 'cool_output',  '" + param.Cool_down_input1.ToString() + "' AS 'cool_down_input',  '" + param.Cool_down_output1.ToString() + "' AS 'cool_down_output',  '" + DateTime.Now.ToString("u") + "' AS 'upd_time',  '1' AS 'machine_num' ,  '" + param.Status_flag1 + "' AS 'machine_num'"
        + " UNION ALL SELECT '" + param.Cool_input2.ToString() + "', '" + param.Cool_output2.ToString() + "',  '" + param.Cool_down_input2.ToString() + "',  '" + param.Cool_down_output2.ToString() + "',  '" + DateTime.Now.ToString("u") + "',  '2', '" + param.Status_flag2 + "' "
        + " UNION ALL SELECT '" + param.Cool_input3.ToString() + "', '" + param.Cool_output3.ToString() + "',  '" + param.Cool_down_input3.ToString() + "',  '" + param.Cool_down_output3.ToString() + "',  '" + DateTime.Now.ToString("u") + "',  '3', '" + param.Status_flag3 + "' "
        ;
        return dbConn.Execute(sql);
    }

    public int AddImmediateData(AllWaterTemp param)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        //param.Status_flag1
        //param.Status_flag2
        //param.Status_flag3
        string sql = @"
        INSERT INTO immediate_temp
        SELECT '" + param.Cool_input1.ToString() + "' AS 'cool_input', '" + param.Cool_output1.ToString() + "' AS 'cool_output',  '" + param.Cool_down_input1.ToString() + "' AS 'cool_down_input',  '" + param.Cool_down_output1.ToString() + "' AS 'cool_down_output',  '" + DateTime.Now.ToString("u") + "' AS 'upd_time',  '1' AS 'machine_num' ,  '" + param.Status_flag1 + "' AS 'machine_num'"
        + " UNION ALL SELECT '" + param.Cool_input2.ToString() + "', '" + param.Cool_output2.ToString() + "',  '" + param.Cool_down_input2.ToString() + "',  '" + param.Cool_down_output2.ToString() + "',  '" + DateTime.Now.ToString("u") + "',  '2', '" + param.Status_flag2 + "' "
        + " UNION ALL SELECT '" + param.Cool_input3.ToString() + "', '" + param.Cool_output3.ToString() + "',  '" + param.Cool_down_input3.ToString() + "',  '" + param.Cool_down_output3.ToString() + "',  '" + DateTime.Now.ToString("u") + "',  '3', '" + param.Status_flag3 + "' "
        ;
        return dbConn.Execute(sql);
    }

    public int DeleteImmediateData()
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"
                    delete from immediate_temp
                    ";
        return dbConn.Execute(sql);
    }

    public int UpdateGate1(string request)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"
                    update gate_setting SET
                    num1 =:request
                    ";
        return dbConn.Execute(sql, new
        {
            request
        });
    }

    public int UpdateGate2(string request)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"
                    update gate_setting SET
                    num2 =:request
                    ";
        return dbConn.Execute(sql, new
        {
            request
        });
    }

    public int UpdateGate3(string request)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"
                    update gate_setting SET
                    num3 =:request
                    ";
        return dbConn.Execute(sql, new
        {
            request
        });
    }
    public int UpdateOffset(OffsetEntity request)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"
                    update offset_setting SET
                    offset1_1 =:Offset1_1,
                    offset1_2 =:Offset1_2,
                    offset1_3 =:Offset1_3,
                    offset1_4 =:Offset1_4,
                    offset2_1 =:Offset2_1,
                    offset2_2 =:Offset2_2,
                    offset2_3 =:Offset2_3,
                    offset2_4 =:Offset2_4,
                    offset3_1 =:Offset3_1,
                    offset3_2 =:Offset3_2,
                    offset3_3 =:Offset3_3,
                    offset3_4 =:Offset3_4
                    ";
        return dbConn.Execute(sql, new
        {
            request.Offset1_1,
            request.Offset1_2,
            request.Offset1_3,
            request.Offset1_4,
            request.Offset2_1,
            request.Offset2_2,
            request.Offset2_3,
            request.Offset2_4,
            request.Offset3_1,
            request.Offset3_2,
            request.Offset3_3,
            request.Offset3_4,
        });
    }
    public int Update(BasicSettingEntity request)
    {
        IDbConnection dbConn = _dbHelper.GetIMSConnection();
        string sql = @"
                    update basic_setting SET
                    port_name =:Port_Name,
                    baud_rate =:Baud_Rate,
                    data_bits =:Data_Bits,
                    stop_bits =:Stop_Bits,
                    parity =:Parity,
                    threshhold_temp_setting1 =:Threshhold_Temp_Setting1,
                    threshhold_temp_setting2 =:Threshhold_Temp_Setting2,
                    threshhold_temp_setting3 =:Threshhold_Temp_Setting3,
                    threshhold_temp_setting10 =:Threshhold_Temp_Setting10,
                    threshhold_temp_setting20 =:Threshhold_Temp_Setting20,
                    threshhold_temp_setting30 =:Threshhold_Temp_Setting30,
                    enable_third_sensor =:Enable_Third_Sensor,
                    usb_dir_location =:Usb_Dir_Location,
                    cold_input1 =:Cold_Input1,
                    cold_output1 =:Cold_Output1,
                    cool_down_input1 =:Cool_Down_Input1,
                    cool_down_output1 =:Cool_Down_Output1,
                    cold_input2 =:Cold_Input2,
                    cold_output2 =:Cold_Output2,
                    cool_down_input2 =:Cool_Down_Input2,
                    cool_down_output2 =:Cool_Down_Output2,
                    cold_input3 =:Cold_Input3,
                    cold_output3 =:Cold_Output3,
                    cool_down_input3 =:Cool_Down_Input3,
                    cool_down_output3 =:Cool_Down_Output3,
                    gate1_open =:Gate1_Open,
                    gate1_close =:Gate1_Close,
                    gate2_open =:Gate2_Open,
                    gate2_close =:Gate2_Close,
                    gate3_open =:Gate3_Open,
                    gate3_close =:Gate3_Close,
                    error_temp1 =:Error_Temp1,
                    error_temp2 =:Error_Temp2,
                    error_temp3 =:Error_Temp3,
                    timer =:Timer,
                    error_down1 =:Error_Down1,
                    error_down2 =:Error_Down2,
                    error_down3 =:Error_Down3,
                    enable_record =:Enable_Record
                    ";
        return dbConn.Execute(sql, new
        {
            request.Port_Name,
            request.Baud_Rate,
            request.Data_Bits,
            request.Stop_Bits,
            request.Parity,
            request.Threshhold_Temp_Setting1,
            request.Threshhold_Temp_Setting2,
            request.Threshhold_Temp_Setting3,
            request.Threshhold_Temp_Setting10,
            request.Threshhold_Temp_Setting20,
            request.Threshhold_Temp_Setting30,
            request.Enable_Third_Sensor,
            request.Usb_Dir_Location,
            request.Cold_Input1,
            request.Cold_Output1,
            request.Cool_Down_Input1,
            request.Cool_Down_Output1,
            request.Cold_Input2,
            request.Cold_Output2,
            request.Cool_Down_Input2,
            request.Cool_Down_Output2,
            request.Cold_Input3,
            request.Cold_Output3,
            request.Cool_Down_Input3,
            request.Cool_Down_Output3,
            request.Gate1_Open,
            request.Gate1_Close,
            request.Gate2_Open,
            request.Gate2_Close,
            request.Gate3_Open,
            request.Gate3_Close,
            request.Error_Temp1,
            request.Error_Temp2,
            request.Error_Temp3,
            request.Timer,
            request.Error_Down1,
            request.Error_Down2,
            request.Error_Down3,
            request.Enable_Record
        });
    }
}