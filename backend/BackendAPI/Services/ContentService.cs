﻿namespace BackendAPI.Services;

public class ContentService : IContentService
{
    private readonly IContentRepository _contentRepository;

    public ContentService(IContentRepository contentRepository)
    {
        this._contentRepository = contentRepository;
    }
    public IList<EChartEntity> QueryAll(string? param1)
    {
        return _contentRepository.QueryAll(param1);
    }
    public EChartEntity QueryLatestOne(string? param1)
    {
        return _contentRepository.QueryLatestOne(param1);
    }
    public EChartEntity ReadLastAvg(string? param1)
    {
        var newestData = _contentRepository.QueryLatestOne(param1);

        var readData = _contentRepository.ReadLastAvg(param1);
        if (readData.Count > 0)
        {
            newestData.Cool_input = readData.Select(x => x.Cool_input).Average();
            newestData.Cool_output = readData.Select(x => x.Cool_output).Average();
            newestData.Cool_down_input = readData.Select(x => x.Cool_down_input).Average();
            newestData.Cool_down_output = readData.Select(x => x.Cool_down_output).Average();
        }
        else
        {
            newestData.Cool_input = 0;
            newestData.Cool_output = 0;
            newestData.Cool_down_input = 0;
            newestData.Cool_down_output = 0;
        }
        return newestData;
    }
    public EChartEntity ReadLastWeekAvg(string? param1)
    {
        var newestData = _contentRepository.QueryLatestOne(param1);
        var readData = _contentRepository.ReadLastWeekAvg(param1);
        if (readData.Count > 0)
        {
            newestData.Cool_input = readData.Select(x => x.Cool_input).Average();
            newestData.Cool_output = readData.Select(x => x.Cool_output).Average();
            newestData.Cool_down_input = readData.Select(x => x.Cool_down_input).Average();
            newestData.Cool_down_output = readData.Select(x => x.Cool_down_output).Average();
        }
        else
        {
            newestData.Cool_input = 0;
            newestData.Cool_output = 0;
            newestData.Cool_down_input = 0;
            newestData.Cool_down_output = 0;
        }
        return newestData;
    }
    public EChartEntity ReadLastMonthAvg(string? param1)
    {
        var newestData = _contentRepository.QueryLatestOne(param1);
        var readData = _contentRepository.ReadLastMonthAvg(param1);
        if (readData.Count > 0)
        {
            newestData.Cool_input = readData.Select(x => x.Cool_input).Average();
            newestData.Cool_output = readData.Select(x => x.Cool_output).Average();
            newestData.Cool_down_input = readData.Select(x => x.Cool_down_input).Average();
            newestData.Cool_down_output = readData.Select(x => x.Cool_down_output).Average();
        }
        else
        {
            newestData.Cool_input = 0;
            newestData.Cool_output = 0;
            newestData.Cool_down_input = 0;
            newestData.Cool_down_output = 0;
        }
        return newestData;
    }
    public IList<ReportEntity> QueryDailayReport(string? machine_num, string? startDate, string? endDate)
    {
        return _contentRepository.QueryDailayReport(machine_num, startDate, endDate);
    }
    public IList<ReportEntity> QueryMonthlyReport(string? machine_num, string? startDate, string? endDate)
    {
        return _contentRepository.QueryMonthlyReport(machine_num, startDate, endDate);
    }
    public IList<ReportEntity> QueryHourlyReport(string? param1, string? startDate, string? endDate)
    {
        return _contentRepository.QueryHourlyReport(param1, startDate, endDate);
    }
    public IList<ReportCoolOutputEntity> QueryCoolOutputReport(string? param1, string? startDate, string? endDate)
    {
        return _contentRepository.QueryCoolOutputReport(param1, startDate, endDate);
    }
    public IList<ReportCoolInputEntity> QueryCoolInputReport(string? param1, string? startDate, string? endDate)
    {
        return _contentRepository.QueryCoolInputReport(param1, startDate, endDate);
    }
    public IList<ReportCoolDownOutputEntity> QueryCoolDownOutputReport(string? param1, string? startDate, string? endDate)
    {
        return _contentRepository.QueryCoolDownOutputReport(param1, startDate, endDate);
    }
    public IList<ReportCoolDownInputEntity> QueryCoolDownInputReport(string? param1, string? startDate, string? endDate)
    {
        return _contentRepository.QueryCoolDownInputReport(param1, startDate, endDate);
    }
    public IList<ReportCoolDiffEntity> QueryCoolDiffReport(string? param1, string? startDate, string? endDate)
    {
        return _contentRepository.QueryCoolDiffReport(param1, startDate, endDate);
    }
    public IList<ReportCoolDownDiffEntity> QueryCoolDownDiffReport(string? param1, string? startDate, string? endDate)
    {
        return _contentRepository.QueryCoolDownDiffReport(param1, startDate, endDate);
    }
}