﻿namespace BackendAPI.Services.IServices;

public interface IContentService
{
    IList<EChartEntity> QueryAll(string? param1);
    EChartEntity QueryLatestOne(string? param1);
    EChartEntity ReadLastAvg(string? param1);
    EChartEntity ReadLastWeekAvg(string? param1);
    EChartEntity ReadLastMonthAvg(string? param1);
    IList<ReportEntity> QueryDailayReport(string? machine_num, string? startDate, string? endDate);
    IList<ReportEntity> QueryMonthlyReport(string? machine_num, string? startDate, string? endDate);
    IList<ReportEntity> QueryHourlyReport(string? machine_num, string? startDate, string? endDate);
    IList<ReportCoolOutputEntity> QueryCoolOutputReport(string? machine_num, string? startDate, string? endDate);
    IList<ReportCoolInputEntity> QueryCoolInputReport(string? machine_num, string? startDate, string? endDate);
    IList<ReportCoolDownOutputEntity> QueryCoolDownOutputReport(string? machine_num, string? startDate, string? endDate);
    IList<ReportCoolDownInputEntity> QueryCoolDownInputReport(string? machine_num, string? startDate, string? endDate);
    IList<ReportCoolDiffEntity> QueryCoolDiffReport(string? machine_num, string? startDate, string? endDate);
    IList<ReportCoolDownDiffEntity> QueryCoolDownDiffReport(string? param1, string? startDate, string? endDate);
}