﻿namespace BackendAPI.Services.IServices;

public interface ISettingService
{
    void Start();
    void TriggerJob();
    void TriggerJobFromWindowsSchedual();
    BasicSettingEntity Query();
    OffsetEntity QueryOffset();
    bool Update(BasicSettingEntity request);
    bool UpdateOffset(OffsetEntity request);
}