﻿namespace BackendAPI.Services;

public class SettingService : ISettingService
{
    private readonly ISettingRepository _settingRepository;
    private readonly ILogger<SettingService> _logger;
    public static SerialPort? SerialPort;
    //利用step區分啟動TriggerJob時，Port_DataReceived方法裡面要分別執行哪些代碼
    public static int step = 0;
    AllWaterTemp allTemp = new AllWaterTemp();
    public static BasicSettingEntity? setting;
    public static decimal offset1_1 = 0;
    public static decimal offset1_2 = 0;
    public static decimal offset1_3 = 0;
    public static decimal offset1_4 = 0;
    public static decimal offset2_1 = 0;
    public static decimal offset2_2 = 0;
    public static decimal offset2_3 = 0;
    public static decimal offset2_4 = 0;
    public static decimal offset3_1 = 0;
    public static decimal offset3_2 = 0;
    public static decimal offset3_3 = 0;
    public static decimal offset3_4 = 0;
    public static int basic_setting_timer = 0;
    public static string status_flag1 = "T";
    public static string status_flag2 = "T";
    public static string status_flag3 = "T";
    public static int status_position = 1;
    //判斷當前代碼執行階段，是否處於執行gate的階段，是的話，就不要在dataReceive繼續做事情
    public static string now_process = "";
    //TriggerJob執行中時，不允許TriggerJobFfromWindows這API執行
    public static bool trigger_job_ing = false;
    public SettingService(ISettingRepository settingRepository, ILogger<SettingService> logger)
    {
        this._settingRepository = settingRepository;
        this._logger = logger;
        Start();
    }
    public bool UpdateOffset(OffsetEntity request)
    {
        var result = _settingRepository.UpdateOffset(request);
        return result > 0 ? true : false;
    }
    public bool Update(BasicSettingEntity request)
    {
        var result = _settingRepository.Update(request);
        return result > 0 ? true : false;
    }
    public OffsetEntity QueryOffset()
    {
        var result = _settingRepository.QueryOffset();
        return result;
    }
    public BasicSettingEntity Query()
    {
        var result = _settingRepository.QueryAll();
        return result;
    }
    public void Start()
    {
        if (SerialPort != null)
        {
            // if (SerialPort.IsOpen)
            // {
            //     SerialPort.Close(); // 關閉串口連接
            // }
            // SerialPort.Dispose(); // 釋放串口資源
            // SerialPort = null; // 幫助垃圾收集
        }
        else
        {
            var offSet = _settingRepository.QueryOffset();
            offset1_1 = offSet.Offset1_1;//冰水出
            offset1_2 = offSet.Offset1_2;//冰水入
            offset1_3 = offSet.Offset1_3;//冷卻水出
            offset1_4 = offSet.Offset1_4;//冷卻水入
            offset2_1 = offSet.Offset2_1;//冰水出
            offset2_2 = offSet.Offset2_2;//冰水入
            offset2_3 = offSet.Offset2_3;//冷卻水出
            offset2_4 = offSet.Offset2_4;//冷卻水入
            offset3_1 = offSet.Offset3_1;//冰水出
            offset3_2 = offSet.Offset3_2;//冰水入
            offset3_3 = offSet.Offset3_3;//冷卻水出
            offset3_4 = offSet.Offset3_4;//冷卻水入
            setting = _settingRepository.QueryAll();
            SerialPort = new SerialPort(setting.Port_Name, setting.Baud_Rate, Enum.Parse<Parity>(setting.Parity), setting.Data_Bits, Enum.Parse<StopBits>(setting.Stop_Bits));
            // //串口名稱
            // SerialPort.PortName = setting.Port_Name;
            // //波特率
            // SerialPort.BaudRate = setting.Baud_Rate;
            // //數據位
            // SerialPort.DataBits = setting.Data_Bits;
            // //停止位
            // SerialPort.StopBits = Enum.Parse<StopBits>(setting.Stop_Bits);
            // //SerialPort.StopBits = StopBits.One;
            // //校驗位
            // SerialPort.Parity = Enum.Parse<Parity>(setting.Parity);

            SerialPort.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
            SerialPort.Open();
        }
    }

    public void TriggerJob()
    {
        try
        {
            trigger_job_ing = true;
            setting = _settingRepository.QueryAll();
            basic_setting_timer = setting.Timer;

            //string cmd = "01-03-00-00-00-01-84-0A";//溫度值
            //string cmd = "09-05-00-00-FF-00-8D-72";//ID 1 繼電器開啟
            //string cmd = "09-05-00-00-00-00-cc-82"; //ID 1 繼電器關閉
            //string cmd = "09-03-00-10-00-01-84-87"; //ID1狀態值
            //string cmd = "09-03-00-11-00-01-d5-47"; //ID2狀態值
            //string cmd = "09-03-00-12-00-01-25-47"; //ID3狀態值

            string cmd = "";
            byte[] hexValues;
            byte[] outBuffer;
            cmd = setting.Status_Input1;
            // cmd = setting.Cold_Input1;
            hexValues = ToByte(cmd);
            outBuffer = hexValues;
            step = 0;

            SerialPort.Write(outBuffer, 0, outBuffer.Length);
            allTemp = new AllWaterTemp();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex.Message);
            if (SerialPort != null)
            {
                if (SerialPort.IsOpen)
                {
                    SerialPort.Close(); // 關閉串口連接
                }
                SerialPort.Dispose(); // 釋放串口資源
                SerialPort = null; // 幫助垃圾收集
            }
        }
    }

    public void TriggerJobFromWindowsSchedual()
    {
        try
        {
            if (trigger_job_ing == false)
            {
                //string cmd = "01-03-00-00-00-01-84-0A";//溫度值
                //string cmd = "09-05-00-00-FF-00-8D-72";//ID 1 繼電器開啟
                //string cmd = "09-05-00-00-00-00-cc-82"; //ID 1 繼電器關閉
                //string cmd = "09-02-00-00-00-08-78-84"; //狀態值

                string cmd = "";
                byte[] hexValues;
                byte[] outBuffer;
                cmd = setting.Cold_Input1;//第一組裝置Sensor設定
                hexValues = ToByte(cmd);
                outBuffer = hexValues;
                SerialPort.Write(outBuffer, 0, outBuffer.Length);
                allTemp = new AllWaterTemp();
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex.Message);
            SerialPort.Close();
            SerialPort.Dispose();
        }
    }

    private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
    {
        if (step == 0)
        {
            if (now_process == "")
            {
                // Show all the incoming data in the port's buffer
                SerialPort sp = (SerialPort)sender;
                int length = sp.BytesToRead;
                byte[] buf = new byte[length];
                sp.Read(buf, 0, length);
                string cmd = "";
                byte[] hexValues;
                byte[] outBuffer;

                if (buf[0].ToString() == "9")
                {
                    if (buf[4].ToString("00") == "01")//代表輸入狀態值是正常的，處理資料時寫入T
                    {
                        if (status_position == 1)//10 Status_Input1
                        {
                            status_flag1 = "T";
                        }
                        else if (status_position == 2)//10 Status_Input2
                        {
                            status_flag2 = "T";
                        }
                        else if (status_position == 3)//10 Status_Input3
                        {
                            status_flag3 = "T";
                        }
                    }
                    if (buf[4].ToString("00") == "00")//代表輸入狀態值是異常的，處理資料時寫入F
                    {
                        if (status_position == 1)//10 Status_Input1
                        {
                            status_flag1 = "F";
                        }
                        else if (status_position == 2)//10 Status_Input2
                        {
                            status_flag2 = "F";
                        }
                        else if (status_position == 3)//10 Status_Input3
                        {
                            status_flag3 = "F";
                        }
                    }
                    if (status_position == 1)//10 Status_Input1
                    {
                        cmd = setting.Status_Input2;
                        hexValues = ToByte(cmd);
                        outBuffer = hexValues;
                        Thread.Sleep(1000);
                        SerialPort.Write(outBuffer, 0, outBuffer.Length);
                    }
                    else if (status_position == 2)//11 Status_Input2
                    {
                        cmd = setting.Status_Input3;
                        hexValues = ToByte(cmd);
                        outBuffer = hexValues;
                        Thread.Sleep(1000);
                        SerialPort.Write(outBuffer, 0, outBuffer.Length);
                    }
                    else if (status_position == 3)//12 Status_Input3，到這個階段之後，輸入狀態值已經全部掃描完畢，開始進行溫度值的讀取
                    {
                        status_position = 1;
                        cmd = setting.Cold_Input1; //第一組裝置Sensor設定
                        hexValues = ToByte(cmd);
                        outBuffer = hexValues;
                        Thread.Sleep(1000);
                        SerialPort.Write(outBuffer, 0, outBuffer.Length);
                    }
                    if (status_position != 3)
                        status_position++;
                    else
                        status_position = 1;
                }
                else
                {
                    //接收17組16進制號碼 ex: 01 03 0C 01 2C 01 2C 19 D2 19 D2 19 D2 19 D2 82 FE
                    //01:ID
                    //03:讀取
                    //0C:功能
                    //01:溫度值-個位數乘256
                    //2C:溫度值-十位數乘16, 個位數就是原本數
                    //計算出的三個溫度值進行加總 256+64+3=323 =>32.3度C
                    //F8:CRC值
                    //25:CRC值
                    //step1 針對接收到的buff資料，先進行初步處理
                    var num1 = Convert.ToDecimal(buf[3].ToString()) * 256;
                    var num2 = Convert.ToDecimal(buf[4].ToString("000"));
                    decimal resultNum1 = (num1 + num2) / 10;

                    var num3 = Convert.ToDecimal(buf[5].ToString()) * 256;
                    var num4 = Convert.ToDecimal(buf[6].ToString("000"));
                    decimal resultNum2 = (num3 + num4) / 10;

                    var num5 = Convert.ToDecimal(buf[7].ToString()) * 256;
                    var num6 = Convert.ToDecimal(buf[8].ToString("000"));
                    decimal resultNum3 = (num5 + num6) / 10;

                    var num7 = Convert.ToDecimal(buf[9].ToString()) * 256;
                    var num8 = Convert.ToDecimal(buf[10].ToString("000"));
                    decimal resultNum4 = (num7 + num8) / 10;

                    var num9 = Convert.ToDecimal(buf[11].ToString()) * 256;
                    var num10 = Convert.ToDecimal(buf[12].ToString("000"));
                    decimal resultNum5 = (num9 + num10) / 10;

                    var num11 = Convert.ToDecimal(buf[13].ToString()) * 256;
                    var num12 = Convert.ToDecimal(buf[14].ToString("000"));
                    decimal resultNum6 = (num11 + num12) / 10;

                    string cmd2 = "";
                    byte[] hexValues2;
                    byte[] outBuffer2;
                    //判斷當前的ID感測器是幾號
                    switch (buf[0].ToString())
                    {
                        case "1":
                            allTemp.Cool_input1 = (resultNum1 + offset1_2) > 500 ? 0 : (resultNum1 + offset1_2);
                            allTemp.Cool_output1 = (resultNum2 + offset1_1) > 500 ? 0 : (resultNum2 + offset1_1);
                            allTemp.Cool_down_input1 = (resultNum3 + offset1_4) > 500 ? 0 : (resultNum3 + offset1_4);
                            allTemp.Cool_down_output1 = (resultNum4 + offset1_3) > 500 ? 0 : (resultNum4 + offset1_3);
                            allTemp.Cool_input2 = (resultNum5 + offset2_2) > 500 ? 0 : (resultNum5 + offset2_2);
                            allTemp.Cool_output2 = (setting.Enable_Record == "open") ? allTemp.Cool_output1 : ((resultNum6 + offset2_1) > 500 ? 0 : (resultNum6 + offset2_1)); //!
                            cmd2 = setting.Cold_Output1; //第二組裝置Sensor設定
                            hexValues2 = ToByte(cmd2);
                            outBuffer2 = hexValues2;
                            Thread.Sleep(1000);
                            SerialPort.Write(outBuffer2, 0, outBuffer2.Length);

                            break;
                        case "2":

                            allTemp.Cool_down_input2 = (resultNum1 + offset2_4) > 500 ? 0 : (resultNum1 + offset2_4);
                            allTemp.Cool_down_output2 = (setting.Enable_Record == "open") ? allTemp.Cool_down_output1 : ((resultNum2 + offset2_3) > 500 ? 0 : (resultNum2 + offset2_3)); //!
                            allTemp.Cool_input3 = (resultNum3 + offset3_2) > 500 ? 0 : (resultNum3 + offset3_2);
                            allTemp.Cool_output3 = (setting.Enable_Record == "open") ? allTemp.Cool_output1 : ((resultNum4 + offset3_1) > 500 ? 0 : (resultNum4 + offset3_1)); //!
                            allTemp.Cool_down_input3 = (resultNum5 + offset3_4) > 500 ? 0 : (resultNum5 + offset3_4);
                            allTemp.Cool_down_output3 = (setting.Enable_Record == "open") ? allTemp.Cool_down_output1 : ((resultNum6 + offset3_3) > 500 ? 0 : (resultNum6 + offset3_3)); //!
                            //這邊要判斷輸入狀態值是否屬於正常的內容，異常的話，代表存入DB的資料要把status_flag設定為F，反之為T
                            allTemp.Status_flag1 = status_flag1;
                            allTemp.Status_flag2 = status_flag2;
                            allTemp.Status_flag3 = status_flag3;
                            //必須是網頁觸發API，寫入water_temp DB，由工作排程器觸發的話，排程是每分鐘讀取一次，寫入到immediate_temp DB，只留最後一筆在DB，提供給即時顯示撈取
                            // if (trigger_from != "windows")
                            if (DateTime.Now.Minute % basic_setting_timer == 0)
                                _settingRepository.AddData(allTemp);

                            _settingRepository.DeleteImmediateData();
                            _settingRepository.AddImmediateData(allTemp);

                            cmd2 = "09-03-00-10-00-01-84-87"; //單純為了呼叫下一個port_DataReceived2
                            hexValues2 = ToByte(cmd2);
                            outBuffer2 = hexValues2;
                            Thread.Sleep(1000);
                            step = 1;
                            SerialPort.Write(outBuffer2, 0, outBuffer2.Length);
                            break;
                    }
                }
            }
            else
            {
                now_process = "";
                return;
            }
        }
        else if (step == 1)
        {
            //判斷是否冷卻水超過設定溫度，超過的話，就啟動繼電器
            if (status_flag1 == "T")
                CheckOverTemp(1, allTemp);
            Thread.Sleep(1000);
            step = 2;
            //單純為了呼叫下一個port_DataReceived3
            string cmd2 = "09-03-00-10-00-01-84-87";
            byte[] hexValues2 = ToByte(cmd2);
            byte[] outBuffer2 = hexValues2;
            SerialPort.Write(outBuffer2, 0, outBuffer2.Length);
        }
        else if (step == 2)
        {
            //判斷是否冷卻水超過設定溫度，超過的話，就啟動繼電器
            if (status_flag2 == "T")
                CheckOverTemp(2, allTemp);
            Thread.Sleep(1000);
            step = 3;
            //單純為了呼叫下一個port_DataReceived4
            string cmd2 = "09-03-00-10-00-01-84-87";
            byte[] hexValues2 = ToByte(cmd2);
            byte[] outBuffer2 = hexValues2;
            SerialPort.Write(outBuffer2, 0, outBuffer2.Length);
        }
        else if (step == 3)
        {
            //判斷是否冷卻水超過設定溫度，超過的話，就啟動繼電器
            if (status_flag3 == "T")
                CheckOverTemp(3, allTemp);
            var gate = _settingRepository.QueryGate();

            now_process = "gate";
            byte[] hexValues;
            byte[] outBuffer;
            string cmd = "";
            if (gate.Num1 == "enable")
            {
                cmd = setting.Gate1_Open; //第一組裝置繼電器開啟
                hexValues = ToByte(cmd);
                outBuffer = hexValues;
                Thread.Sleep(1000);
                step = 4;
                SerialPort.Write(outBuffer, 0, outBuffer.Length);
            }
            else
            {
                cmd = setting.Gate1_Close; //第一組裝置繼電器關閉
                hexValues = ToByte(cmd);
                outBuffer = hexValues;
                Thread.Sleep(1000);
                step = 4;
                SerialPort.Write(outBuffer, 0, outBuffer.Length);
            }
        }
        else if (step == 4)
        {
            var gate = _settingRepository.QueryGate();
            now_process = "gate";
            byte[] hexValues;
            byte[] outBuffer;
            string cmd = "";
            if (gate.Num2 == "enable")
            {
                cmd = setting.Gate2_Open; //第二組裝置繼電器開啟
                hexValues = ToByte(cmd);
                outBuffer = hexValues;
                Thread.Sleep(1000);
                step = 5;
                SerialPort.Write(outBuffer, 0, outBuffer.Length);
            }
            else
            {
                cmd = setting.Gate2_Close; //第二組裝置繼電器關閉
                hexValues = ToByte(cmd);
                outBuffer = hexValues;
                Thread.Sleep(1000);
                step = 5;
                SerialPort.Write(outBuffer, 0, outBuffer.Length);
            }
        }
        else if (step == 5)
        {
            var gate = _settingRepository.QueryGate();
            now_process = "gate";
            byte[] hexValues;
            byte[] outBuffer;
            string cmd = "";
            if (gate.Num3 == "enable")
            {
                cmd = setting.Gate3_Open; //第三組裝置繼電器開啟
                hexValues = ToByte(cmd);
                outBuffer = hexValues;
                Thread.Sleep(1000);
                step = 6;
                SerialPort.Write(outBuffer, 0, outBuffer.Length);
            }
            else
            {
                cmd = setting.Gate3_Close; //第三組裝置繼電器關閉
                hexValues = ToByte(cmd);
                outBuffer = hexValues;
                Thread.Sleep(1000);
                step = 6;
                SerialPort.Write(outBuffer, 0, outBuffer.Length);
            }
        }
        else if (step == 6)
        {
            now_process = "";
            trigger_job_ing = false;
        }
    }

    /// <summary>
    /// 寫入DB後，判斷是否冷卻水超過設定溫度，超過的話，就啟動繼電器
    /// </summary>
    /// <param name="machine_num"></param>
    /// <param name="temp"></param>
    /// <returns></returns>
    private void CheckOverTemp(int machine_num, AllWaterTemp allTemp)
    {
        var gate = _settingRepository.QueryGate();
        decimal Minthreshhold_temp = 0;
        decimal Maxthreshhold_temp = 0;
        if (machine_num == 1)
        {
            Minthreshhold_temp = setting.Threshhold_Temp_Setting1; //第一組冷卻水入水觸發繼電器最小溫度值
            Maxthreshhold_temp = setting.Threshhold_Temp_Setting10; //第一組冷卻水入水觸發繼電器最大溫度值
            if (gate.Num1 == "disable" && allTemp.Cool_down_input1 >= Maxthreshhold_temp)
            {
                _settingRepository.UpdateGate1("enable");
            }

            if (gate.Num1 == "enable" && allTemp.Cool_down_input1 <= Minthreshhold_temp)
            {
                _settingRepository.UpdateGate1("disable");
            }
        }
        if (machine_num == 2)
        {
            Minthreshhold_temp = setting.Threshhold_Temp_Setting2; //第二組冷卻水入水觸發繼電器溫度值
            Maxthreshhold_temp = setting.Threshhold_Temp_Setting20; //第二組冷卻水入水觸發繼電器最大溫度值
            if (gate.Num2 == "disable" && allTemp.Cool_down_input2 >= Maxthreshhold_temp)
            {
                _settingRepository.UpdateGate2("enable");
            }
            if (gate.Num2 == "enable" && allTemp.Cool_down_input2 <= Minthreshhold_temp)
            {
                _settingRepository.UpdateGate2("disable");
            }
        }
        if (machine_num == 3)
        {
            Minthreshhold_temp = setting.Threshhold_Temp_Setting3; //第三組冷卻水入水觸發繼電器溫度值
            Maxthreshhold_temp = setting.Threshhold_Temp_Setting30; //第三組冷卻水入水觸發繼電器最大溫度值
            if (gate.Num3 == "disable" && allTemp.Cool_down_input3 >= Maxthreshhold_temp)
            {
                _settingRepository.UpdateGate3("enable");
            }

            if (gate.Num3 == "enable" && allTemp.Cool_down_input3 <= Minthreshhold_temp)
            {
                _settingRepository.UpdateGate3("disable");
            }
        }
    }

    /// <summary>
    /// 把按-分割的字符串轉成byte[]
    /// </summary>
    /// <param name="s">string原數據</param>
    /// <returns></returns>
    public byte[] ToByte(string s)
    {
        return s.Split('-').AsParallel().Select(x => Convert.ToByte(x, 16)).ToArray();
    }
}