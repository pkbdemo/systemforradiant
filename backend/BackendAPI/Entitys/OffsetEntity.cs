﻿namespace BackendAPI.Entitys;

public class OffsetEntity
{
    public decimal Offset1_1 { get; set; }
    public decimal Offset1_2 { get; set; }
    public decimal Offset1_3 { get; set; }
    public decimal Offset1_4 { get; set; }
    public decimal Offset2_1 { get; set; }
    public decimal Offset2_2 { get; set; }
    public decimal Offset2_3 { get; set; }
    public decimal Offset2_4 { get; set; }
    public decimal Offset3_1 { get; set; }
    public decimal Offset3_2 { get; set; }
    public decimal Offset3_3 { get; set; }
    public decimal Offset3_4 { get; set; }

}