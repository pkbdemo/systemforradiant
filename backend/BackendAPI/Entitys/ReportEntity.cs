namespace BackendAPI.Entitys;

public class ReportEntity
{
    public decimal Cool_input { get; set; }
    public decimal Cool_output { get; set; }
    public decimal Cool_temp_diff { get; set; }
    public decimal Cool_down_input { get; set; }
    public decimal Cool_down_output { get; set; }
    public decimal Cool_down_temp_diff { get; set; }
    public string Upd_time { get; set; }
    public int Machine_num { get; set; }
}

public class ReportCoolOutputEntity
{
    public decimal Cool_output { get; set; }
    public string Upd_time { get; set; }
}
public class ReportCoolInputEntity
{
    public decimal Cool_input { get; set; }
    public string Upd_time { get; set; }
}
public class ReportCoolDownOutputEntity
{
    public decimal Cool_down_output { get; set; }
    public string Upd_time { get; set; }
}
public class ReportCoolDownInputEntity
{
    public decimal Cool_down_input { get; set; }
    public string Upd_time { get; set; }
}
public class ReportCoolDiffEntity
{
    public decimal Cool_diff { get; set; }
    public string Upd_time { get; set; }
}
public class ReportCoolDownDiffEntity
{
    public decimal Cool_down_diff { get; set; }
    public string Upd_time { get; set; }
}