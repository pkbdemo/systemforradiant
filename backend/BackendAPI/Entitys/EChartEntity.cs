﻿namespace BackendAPI.Entitys;

public class EChartEntity
{
    public decimal Cool_input { get; set; }
    public decimal Cool_output { get; set; }
    public decimal Cool_down_input { get; set; }
    public decimal Cool_down_output { get; set; }
    public string Upd_time { get; set; }
    public int Machine_num { get; set; }
    public string Status_flag { get; set; }
}