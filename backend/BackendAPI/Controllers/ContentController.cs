﻿using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

namespace BackendAPI.Controllers;

[Route(IMSConstants.ControllerRoute)]
[Produces(IMSConstants.ControllerProducesContentType)]
public class ContentController : ControllerBase
{
    private readonly IContentService _contentService;
    private readonly ILogger<ContentController> _logger;
    private readonly ISettingRepository _settingRepository;

    public ContentController(IContentService contentService, ILogger<ContentController> logger, ISettingRepository settingRepository)
    {
        this._contentService = contentService;
        this._logger = logger;
        this._settingRepository = settingRepository;
    }

    [HttpPost]
    public ActionResult<IList<EChartEntity>> QueryAll(string? param1)
    {
        _logger.LogInformation("API:QueryAll Start");
        var result = _contentService.QueryAll(param1);
        _logger.LogInformation("API:QueryAll End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<EChartEntity> QueryLatestOne(string? param1)
    {
        _logger.LogInformation("API:QueryLatestOne Start");
        var result = _contentService.QueryLatestOne(param1);
        _logger.LogInformation("API:QueryLatestOne End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<EChartEntity> ReadLastAvg(string? param1)
    {
        _logger.LogInformation("API:ReadLastAvg Start");
        var result = _contentService.ReadLastAvg(param1);
        _logger.LogInformation("API:ReadLastAvg End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<EChartEntity> ReadLastWeekAvg(string? param1)
    {
        _logger.LogInformation("API:ReadLastWeekAvg Start");
        var result = _contentService.ReadLastWeekAvg(param1);
        _logger.LogInformation("API:ReadLastWeekAvg End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<EChartEntity> ReadLastMonthAvg(string? param1)
    {
        _logger.LogInformation("API:ReadLastMonthAvg Start");
        var result = _contentService.ReadLastMonthAvg(param1);
        _logger.LogInformation("API:ReadLastMonthAvg End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<ReportEntity> QueryDailayReport(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:QueryDailayReport Start");
        var result = _contentService.QueryDailayReport(param1, startDate, endDate);
        _logger.LogInformation("API: End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<ReportEntity> QueryMonthlyReport(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:QueryMonthlyReport Start");
        var result = _contentService.QueryMonthlyReport(param1, startDate, endDate);
        _logger.LogInformation("API:QueryMonthlyReport End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<ReportEntity> QueryHourlyReport(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:QueryHourlyReport Start");
        var result = _contentService.QueryHourlyReport(param1, startDate, endDate);
        _logger.LogInformation("API:QueryHourlyReport End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<ReportEntity> QueryCoolOutputReport(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:QueryCoolOutputReport Start");
        var result = _contentService.QueryCoolOutputReport(param1, startDate, endDate);
        _logger.LogInformation("API:QueryCoolOutputReport End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<ReportEntity> QueryCoolInputReport(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:QueryCoolInputReport Start");
        var result = _contentService.QueryCoolInputReport(param1, startDate, endDate);
        _logger.LogInformation("API:QueryCoolInputReport End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<ReportEntity> QueryCoolDownOutputReport(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:QueryCoolDownOutputReport Start");
        var result = _contentService.QueryCoolDownOutputReport(param1, startDate, endDate);
        _logger.LogInformation("API:QueryCoolDownOutputReport End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<ReportEntity> QueryCoolDownInputReport(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:QueryCoolDownInputReport Start");
        var result = _contentService.QueryCoolDownInputReport(param1, startDate, endDate);
        _logger.LogInformation("API:QueryCoolDownInputReport End");
        return Ok(result);
    }


    [HttpPost]
    public ActionResult<ReportEntity> QueryCoolDiffReport(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:QueryCoolDiffReport Start");
        var result = _contentService.QueryCoolDiffReport(param1, startDate, endDate);
        _logger.LogInformation("API:QueryCoolDiffReport End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<ReportEntity> QueryCoolDownDiffReport(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:QueryCoolDownDiffReport Start");
        var result = _contentService.QueryCoolDownDiffReport(param1, startDate, endDate);
        _logger.LogInformation("API:QueryCoolDownDiffReport End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<ReportEntity> DownloadMonthlyExcel(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:DownloadMonthlyExcel Start");
        var settingList = _settingRepository.QueryAll();
        var result = _contentService.QueryMonthlyReport(param1, startDate, endDate);
        string filePath = string.Empty;
        HSSFWorkbook workbook = null;//Excel实例
        ISheet sheet1 = null;//表实例
        IRow row = null; //行
        ISheet sheet2 = null;//表实例
        IRow row2 = null; //行
        int nowRowNum = 1;//当前行2,表头第一行

        //模板路径    打开模板  bin\Debug\temp\Excels.xls
        string excelTempPath = System.Environment.CurrentDirectory + @"\0003.xls";
        //string excelTempPath = "C:/Users/ironman/Desktop/WorkFiles/Excels.xls";

        //读取Excel模板
        using (FileStream fs = new FileStream(excelTempPath, FileMode.Open, FileAccess.Read))
        {
            workbook = new HSSFWorkbook(fs);
        }

        //获取sheet1
        sheet1 = workbook.GetSheetAt(0);
        for (int i = 0; i < result.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            row.CreateCell(0).SetCellValue(result[i].Upd_time);
            row.CreateCell(1).SetCellValue(Convert.ToDouble(result[i].Cool_output));
            row.CreateCell(2).SetCellValue(Convert.ToDouble(result[i].Cool_input));
            row.CreateCell(3).SetCellValue(Convert.ToDouble(result[i].Cool_temp_diff));
            row.CreateCell(4).SetCellValue(Convert.ToDouble(result[i].Cool_down_output));
            row.CreateCell(5).SetCellValue(Convert.ToDouble(result[i].Cool_down_input));
            row.CreateCell(6).SetCellValue(Convert.ToDouble(result[i].Cool_down_temp_diff));
        }

        //获取sheet2
        sheet2 = workbook.GetSheetAt(1);
        row2 = sheet2.CreateRow(0);
        row2.CreateCell(0).SetCellValue("總筆數");
        row2 = sheet2.CreateRow(1);
        row2.CreateCell(0).SetCellValue(result.Count);



        FileStream file = new FileStream(settingList.Usb_Dir_Location + @"\MonthlyReport_Num" + param1 + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls", FileMode.Create);
        workbook.Write(file);
        file.Close();

        _logger.LogInformation("API:DownloadMonthlyExcel End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<ReportEntity> DownloadDailyExcel(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:DownloadDailyExcel Start");
        var settingList = _settingRepository.QueryAll();
        var result = _contentService.QueryDailayReport(param1, startDate, endDate);
        string filePath = string.Empty;
        HSSFWorkbook workbook = null;//Excel实例
        ISheet sheet1 = null;//表实例
        IRow row = null; //行
        ISheet sheet2 = null;//表实例
        IRow row2 = null; //行
        int nowRowNum = 1;//当前行2,表头第一行

        //模板路径    打开模板  bin\Debug\temp\Excels.xls
        string excelTempPath = System.Environment.CurrentDirectory + @"\0003.xls";
        //string excelTempPath = "C:/Users/ironman/Desktop/WorkFiles/Excels.xls";

        //读取Excel模板
        using (FileStream fs = new FileStream(excelTempPath, FileMode.Open, FileAccess.Read))
        {
            workbook = new HSSFWorkbook(fs);
        }

        //获取sheet1
        sheet1 = workbook.GetSheetAt(0);
        for (int i = 0; i < result.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            row.CreateCell(0).SetCellValue(result[i].Upd_time);
            row.CreateCell(1).SetCellValue(Convert.ToDouble(result[i].Cool_output));
            row.CreateCell(2).SetCellValue(Convert.ToDouble(result[i].Cool_input));
            row.CreateCell(3).SetCellValue(Convert.ToDouble(result[i].Cool_temp_diff));
            row.CreateCell(4).SetCellValue(Convert.ToDouble(result[i].Cool_down_output));
            row.CreateCell(5).SetCellValue(Convert.ToDouble(result[i].Cool_down_input));
            row.CreateCell(6).SetCellValue(Convert.ToDouble(result[i].Cool_down_temp_diff));
        }

        //获取sheet2
        sheet2 = workbook.GetSheetAt(1);
        row2 = sheet2.CreateRow(0);
        row2.CreateCell(0).SetCellValue("總筆數");
        row2 = sheet2.CreateRow(1);
        row2.CreateCell(0).SetCellValue(result.Count);

        FileStream file = new FileStream(settingList.Usb_Dir_Location + @"\DailyReport_Num" + param1 + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls", FileMode.Create);
        workbook.Write(file);
        file.Close();

        _logger.LogInformation("API:DownloadDailyExcel End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<ReportEntity> DownloadHourlyExcel(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:DownloadHourlyExcel Start");
        var settingList = _settingRepository.QueryAll();
        var result = _contentService.QueryHourlyReport(param1, startDate, endDate);
        string filePath = string.Empty;
        HSSFWorkbook workbook = null;//Excel实例
        ISheet sheet1 = null;//表实例
        IRow row = null; //行
        ISheet sheet2 = null;//表实例
        IRow row2 = null; //行
        int nowRowNum = 1;//当前行2,表头第一行

        //模板路径    打开模板  bin\Debug\temp\Excels.xls
        string excelTempPath = System.Environment.CurrentDirectory + @"\0003.xls";
        //string excelTempPath = "C:/Users/ironman/Desktop/WorkFiles/Excels.xls";

        //读取Excel模板
        using (FileStream fs = new FileStream(excelTempPath, FileMode.Open, FileAccess.Read))
        {
            workbook = new HSSFWorkbook(fs);
        }

        //获取sheet1
        sheet1 = workbook.GetSheetAt(0);
        for (int i = 0; i < result.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            row.CreateCell(0).SetCellValue(result[i].Upd_time);
            row.CreateCell(1).SetCellValue(Convert.ToDouble(result[i].Cool_output));
            row.CreateCell(2).SetCellValue(Convert.ToDouble(result[i].Cool_input));
            row.CreateCell(3).SetCellValue(Convert.ToDouble(result[i].Cool_temp_diff));
            row.CreateCell(4).SetCellValue(Convert.ToDouble(result[i].Cool_down_output));
            row.CreateCell(5).SetCellValue(Convert.ToDouble(result[i].Cool_down_input));
            row.CreateCell(6).SetCellValue(Convert.ToDouble(result[i].Cool_down_temp_diff));
        }

        //获取sheet2
        sheet2 = workbook.GetSheetAt(1);
        row2 = sheet2.CreateRow(0);
        row2.CreateCell(0).SetCellValue("總筆數");
        row2 = sheet2.CreateRow(1);
        row2.CreateCell(0).SetCellValue(result.Count);

        FileStream file = new FileStream(settingList.Usb_Dir_Location + @"\HourlyReport_Num" + param1 + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls", FileMode.Create);
        workbook.Write(file);
        file.Close();

        _logger.LogInformation("API:DownloadHourlyExcel End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<ReportCoolOutputEntity> DownloadCoolOutputExcel(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:DownloadCoolOutputExcel Start");
        var settingList = _settingRepository.QueryAll();
        var result = _contentService.QueryCoolOutputReport(param1, startDate, endDate);
        string filePath = string.Empty;
        XSSFWorkbook workbook = null;//Excel实例
        ISheet sheet1 = null;//表实例
        ISheet sheet2 = null;//表实例
        IRow row = null; //
        IRow row2 = null; //
        int nowRowNum = 1;//当前行2,表头第一行

        //模板路径    打开模板  bin\Debug\temp\Excels.xls
        string excelTempPath = System.Environment.CurrentDirectory + @"\cool_output.xlsx";
        //string excelTempPath = "C:/Users/ironman/Desktop/WorkFiles/Excels.xls";

        //读取Excel模板
        using (FileStream fs = new FileStream(excelTempPath, FileMode.Open, FileAccess.Read))
        {
            workbook = new XSSFWorkbook(fs);
        }
        //获取sheet1
        sheet1 = workbook.GetSheetAt(0);
        for (int i = 0; i < result.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            row.CreateCell(0).SetCellValue(result[i].Upd_time);
            row.CreateCell(1).SetCellValue(Convert.ToDouble(result[i].Cool_output));
        }

        //获取sheet2
        sheet2 = workbook.GetSheetAt(1);
        row2 = sheet2.CreateRow(0);
        row2.CreateCell(0).SetCellValue("總筆數");
        row2 = sheet2.CreateRow(1);
        row2.CreateCell(0).SetCellValue(result.Count);
        FileStream file = new FileStream(settingList.Usb_Dir_Location + @"\Cool_Output_Report_Num" + param1 + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx", FileMode.Create);
        workbook.Write(file);
        file.Close();
        _logger.LogInformation("API:DownloadCoolOutputExcel End");
        return Ok(result);
    }
    [HttpPost]
    public ActionResult<ReportCoolOutputEntity> DownloadCoolInputExcel(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:DownloadCoolInputExcel Start");
        var settingList = _settingRepository.QueryAll();
        var result = _contentService.QueryCoolInputReport(param1, startDate, endDate);
        string filePath = string.Empty;
        XSSFWorkbook workbook = null;//Excel实例
        ISheet sheet1 = null;//表实例
        IRow row = null; //行
        ISheet sheet2 = null;//表实例
        IRow row2 = null; //行
        int nowRowNum = 1;//当前行2,表头第一行

        //模板路径    打开模板  bin\Debug\temp\Excels.xls
        string excelTempPath = System.Environment.CurrentDirectory + @"\cool_input.xlsx";

        //读取Excel模板
        using (FileStream fs = new FileStream(excelTempPath, FileMode.Open, FileAccess.Read))
        {
            workbook = new XSSFWorkbook(fs);
        }

        //获取sheet1
        sheet1 = workbook.GetSheetAt(0);
        for (int i = 0; i < result.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            row.CreateCell(0).SetCellValue(result[i].Upd_time);
            row.CreateCell(1).SetCellValue(Convert.ToDouble(result[i].Cool_input));
        }

        //获取sheet2
        sheet2 = workbook.GetSheetAt(1);
        row2 = sheet2.CreateRow(0);
        row2.CreateCell(0).SetCellValue("總筆數");
        row2 = sheet2.CreateRow(1);
        row2.CreateCell(0).SetCellValue(result.Count);

        FileStream file = new FileStream(settingList.Usb_Dir_Location + @"\Cool_Input_Report_Num" + param1 + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx", FileMode.Create);
        workbook.Write(file);
        file.Close();
        _logger.LogInformation("API:DownloadCoolInputExcel End");
        return Ok(result);
    }
    [HttpPost]
    public ActionResult<ReportCoolOutputEntity> DownloadCoolDownOutputExcel(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:DownloadCoolDownOutputExcel Start");
        var settingList = _settingRepository.QueryAll();
        var result = _contentService.QueryCoolDownOutputReport(param1, startDate, endDate);
        string filePath = string.Empty;
        XSSFWorkbook workbook = null;//Excel实例
        ISheet sheet1 = null;//表实例
        IRow row = null; //行
        ISheet sheet2 = null;//表实例
        IRow row2 = null; //行
        int nowRowNum = 1;//当前行2,表头第一行

        //模板路径    打开模板  bin\Debug\temp\Excels.xls
        string excelTempPath = System.Environment.CurrentDirectory + @"\cool_down_output.xlsx";
        //string excelTempPath = "C:/Users/ironman/Desktop/WorkFiles/Excels.xls";

        //读取Excel模板
        using (FileStream fs = new FileStream(excelTempPath, FileMode.Open, FileAccess.Read))
        {
            workbook = new XSSFWorkbook(fs);
        }

        //获取sheet1
        sheet1 = workbook.GetSheetAt(0);
        for (int i = 0; i < result.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            row.CreateCell(0).SetCellValue(result[i].Upd_time);
            row.CreateCell(1).SetCellValue(Convert.ToDouble(result[i].Cool_down_output));
        }

        //获取sheet2
        sheet2 = workbook.GetSheetAt(1);
        row2 = sheet2.CreateRow(0);
        row2.CreateCell(0).SetCellValue("總筆數");
        row2 = sheet2.CreateRow(1);
        row2.CreateCell(0).SetCellValue(result.Count);

        FileStream file = new FileStream(settingList.Usb_Dir_Location + @"\Cool_Down_Output_Report_Num" + param1 + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx", FileMode.Create);
        workbook.Write(file);
        file.Close();
        _logger.LogInformation("API:DownloadCoolDownOutputExcel End");
        return Ok(result);
    }
    [HttpPost]
    public ActionResult<ReportCoolOutputEntity> DownloadCoolDownInputExcel(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:DownloadCoolDownInputExcel Start");
        var settingList = _settingRepository.QueryAll();
        var result = _contentService.QueryCoolDownInputReport(param1, startDate, endDate);
        string filePath = string.Empty;
        XSSFWorkbook workbook = null;//Excel实例
        ISheet sheet1 = null;//表实例
        IRow row = null; //行
        ISheet sheet2 = null;//表实例
        IRow row2 = null; //行
        int nowRowNum = 1;//当前行2,表头第一行

        //模板路径    打开模板  bin\Debug\temp\Excels.xls
        string excelTempPath = System.Environment.CurrentDirectory + @"\cool_down_input.xlsx";
        //string excelTempPath = "C:/Users/ironman/Desktop/WorkFiles/Excels.xls";

        //读取Excel模板
        using (FileStream fs = new FileStream(excelTempPath, FileMode.Open, FileAccess.Read))
        {
            workbook = new XSSFWorkbook(fs);
        }

        //获取sheet1
        sheet1 = workbook.GetSheetAt(0);
        for (int i = 0; i < result.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            row.CreateCell(0).SetCellValue(result[i].Upd_time);
            row.CreateCell(1).SetCellValue(Convert.ToDouble(result[i].Cool_down_input));
        }

        //获取sheet2
        sheet2 = workbook.GetSheetAt(1);
        row2 = sheet2.CreateRow(0);
        row2.CreateCell(0).SetCellValue("總筆數");
        row2 = sheet2.CreateRow(1);
        row2.CreateCell(0).SetCellValue(result.Count);

        FileStream file = new FileStream(settingList.Usb_Dir_Location + @"\Cool_Down_Input_Report_Num" + param1 + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx", FileMode.Create);
        workbook.Write(file);
        file.Close();
        _logger.LogInformation("API:DownloadCoolDownInputExcel End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<ReportCoolDiffEntity> DownloadCoolDiffExcel(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:DownloadCoolDiffExcel Start");
        var settingList = _settingRepository.QueryAll();
        var result = _contentService.QueryCoolDiffReport(param1, startDate, endDate);
        string filePath = string.Empty;
        XSSFWorkbook workbook = null;//Excel实例
        ISheet sheet1 = null;//表实例
        IRow row = null; //行
        ISheet sheet2 = null;//表实例
        IRow row2 = null; //行
        int nowRowNum = 1;//当前行2,表头第一行

        //模板路径    打开模板  bin\Debug\temp\Excels.xls
        string excelTempPath = System.Environment.CurrentDirectory + @"\cool_diff.xlsx";
        //string excelTempPath = "C:/Users/ironman/Desktop/WorkFiles/Excels.xls";

        //读取Excel模板
        using (FileStream fs = new FileStream(excelTempPath, FileMode.Open, FileAccess.Read))
        {
            workbook = new XSSFWorkbook(fs);
        }
        //获取sheet1
        sheet1 = workbook.GetSheetAt(0);
        for (int i = 0; i < result.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            row.CreateCell(0).SetCellValue(result[i].Upd_time);
            row.CreateCell(1).SetCellValue(Convert.ToDouble(result[i].Cool_diff));
        }

        //获取sheet2
        sheet2 = workbook.GetSheetAt(1);
        row2 = sheet2.CreateRow(0);
        row2.CreateCell(0).SetCellValue("總筆數");
        row2 = sheet2.CreateRow(1);
        row2.CreateCell(0).SetCellValue(result.Count);

        FileStream file = new FileStream(settingList.Usb_Dir_Location + @"\Cool_Diff_Report_Num" + param1 + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx", FileMode.Create);
        workbook.Write(file);
        file.Close();
        _logger.LogInformation("API:DownloadCoolDiffExcel End");
        return Ok(result);
    }
    [HttpPost]
    public ActionResult<ReportCoolDownDiffEntity> DownloadCoolDownDiffExcel(string? param1, string? startDate, string? endDate)
    {
        _logger.LogInformation("API:DownloadCoolDownDiffExcel Start");
        var settingList = _settingRepository.QueryAll();
        var result = _contentService.QueryCoolDownDiffReport(param1, startDate, endDate);
        string filePath = string.Empty;
        XSSFWorkbook workbook = null;//Excel实例
        ISheet sheet1 = null;//表实例
        IRow row = null; //行
        ISheet sheet2 = null;//表实例
        IRow row2 = null; //行
        int nowRowNum = 1;//当前行2,表头第一行

        //模板路径    打开模板  bin\Debug\temp\Excels.xls
        string excelTempPath = System.Environment.CurrentDirectory + @"\cool_down_diff.xlsx";
        //string excelTempPath = "C:/Users/ironman/Desktop/WorkFiles/Excels.xls";

        //读取Excel模板
        using (FileStream fs = new FileStream(excelTempPath, FileMode.Open, FileAccess.Read))
        {
            workbook = new XSSFWorkbook(fs);
        }

        //获取sheet1
        sheet1 = workbook.GetSheetAt(0);
        for (int i = 0; i < result.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            row.CreateCell(0).SetCellValue(result[i].Upd_time);
            row.CreateCell(1).SetCellValue(Convert.ToDouble(result[i].Cool_down_diff));
        }

        //获取sheet2
        sheet2 = workbook.GetSheetAt(1);
        row2 = sheet2.CreateRow(0);
        row2.CreateCell(0).SetCellValue("總筆數");
        row2 = sheet2.CreateRow(1);
        row2.CreateCell(0).SetCellValue(result.Count);

        FileStream file = new FileStream(settingList.Usb_Dir_Location + @"\Cool_Down_Diff_Report_Num" + param1 + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx", FileMode.Create);
        workbook.Write(file);
        file.Close();
        _logger.LogInformation("API:DownloadCoolDownDiffExcel End");
        return Ok(result);
    }
}