﻿namespace BackendAPI.Controllers;

[Route(IMSConstants.ControllerRoute)]
[Produces(IMSConstants.ControllerProducesContentType)]
public class SettingController : ControllerBase
{
    private readonly ISettingService _settingService;
    private readonly ILogger<SettingController> _logger;

    public SettingController(ISettingService settingService, ILogger<SettingController> logger)
    {
        this._settingService = settingService;
        this._logger = logger;
    }

    // [HttpPost]
    // public ActionResult<string> StartTimer()
    // {
    //     _settingService.StartTimer();
    //     return Ok();
    // }

    [HttpGet]
    public ActionResult<BasicSettingEntity> TriggerJob()
    {
         _logger.LogInformation("API:TriggerJob Start");
        _settingService.TriggerJob();
         _logger.LogInformation("API:TriggerJob End");
        return Ok();
    }

     [HttpGet]
    public ActionResult<BasicSettingEntity> TriggerJobFromWindowsSchedual()
    {
         _logger.LogInformation("API:TriggerJobFromWindowsSchedual Start");
        _settingService.TriggerJobFromWindowsSchedual();
         _logger.LogInformation("API:TriggerJobFromWindowsSchedual End");
        return Ok();
    }

    [HttpPost]
    public ActionResult<BasicSettingEntity> Query(string? param1)
    {
         _logger.LogInformation("API:Query Start");
        var result = _settingService.Query();
         _logger.LogInformation("API:Query End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<bool> Update(BasicSettingEntity request)
    {
         _logger.LogInformation("API:Update Start");
        var result = _settingService.Update(request);
         _logger.LogInformation("API:Update End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<OffsetEntity> QueryOffset(string? param1)
    {
         _logger.LogInformation("API:QueryOffset Start");
        var result = _settingService.QueryOffset();
         _logger.LogInformation("API:QueryOffset End");
        return Ok(result);
    }

    [HttpPost]
    public ActionResult<bool> UpdateOffset(OffsetEntity request)
    {
         _logger.LogInformation("API:UpdateOffset Start");
        var result = _settingService.UpdateOffset(request);
         _logger.LogInformation("API:UpdateOffset End");
        return Ok(result);
    }

}